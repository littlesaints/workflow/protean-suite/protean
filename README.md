# [Protean](https://www.lexico.com/en/definition/protean)

[![wiki][wiki_badge]][wiki] [![javadoc][api_badge]][api] [![build][build_badge]][build] [![coverage][coverage_badge]][coverage] [![tests][tests_badge]][tests] [![license][license_badge]][lgplv3]

[![maven-central_core][maven-central_badge]][latest-release-core] [![maven-central_spring][maven-central_badge]][latest-release-spring]

## Contents
- [What is Protean ?](#what-is-protean-)
- [Why should I use Protean ?](#why-should-i-use-protean-)
- [Features](#features)
- [Documentation](#documentation)
- [Code Samples](#code-samples)
- [Usage](#usage)
- [License](#license)

## What is Protean ?

[Protean](https://www.lexico.com/en/definition/protean) is a event driven framework to design application as data processing pipelines or workflow using **json / yaml** format and in an **implementation agnostic** manner. 

It enables you to view your application workflow as a **Graph** of interconnected nodes a.k.a. stages.
Protean wires your business logic on an exiting framework, without you having to work with it's DSL i.e. only with configuration.
Designing the application as a [DAG](https://en.wikipedia.org/wiki/Directed_acyclic_graph) allows you to easily modify the application flow.

![DAG](docs/images/DAG.png)

This framework is an abstraction over other popular frameworks to support common use cases, features and best practices. 
Protean ensures that your application is enriched with capabilities required to be robust, performant and *protean*.

The features available in Protean are either unique or not available in a single framework.

You can design your application into stages and integrate them as java functions. 
Then, you can configure the application flow i.e. the dependencies among the stages and other capabilities in a json / yaml.
Next, you can configure additional features and manage various models for concurrency.

The framework itself is designed using plugged in functions, allowing the application to (optionally) extend and/or customize any functionality or an internal component.
Hence, the application can choose to modify Protean as required.

[sample configuration](docs/processing.json)

[Make your resources available to Protean](https://gitlab.com/littlesaints/workflow/protean-suite/wikis/configuration#choose-a-resource-locator)

### Supported Executors
- Java 8 streams
- LMAX Disruptor
- Synchronous (Core Java) - The application executes the business logic without using any other framework. 
This means synchronous execution of the your code by the application thread.

### Features

- Concurrency models
    - Thread based
    - Task based
 

- Processing models
    - Ordered
        Incoming data / requests are processed in the order in which they're submitted for processing.
    - Unordered
        No processing order is maintained among incoming data / requests.
    - Content based Order
        Application defines the logic to derive the co-relation (route key) from the incoming data / requests. 
        Protean ensures in-order processing for co-related data / requests.
 

- Throttle

    Protean will configure your application to stop intake if upper bound for your throttle is breached and will turns it on once it's below the lower bound.
    Currently supported  throttles are:
    - Size - Limit the number of events/messages/requests processed at a time.
    - *Memory* - Define an upper and lower bound on memory utilization. 
    Protean will configure your application to stop intake if upper bound is breached and will turns it on once it's below the lower bound.
    - Size and Memory (Combined)
 

- Automatic Retries

    Once you decompose your application as stages, Protean provides for *configuring* Retry logic to a stage.
    This wraps your execution with a phased back-off retry logic, in case the execution fails and needs to be retried. 
    The predicate to decide whether an execution is retried, is provided by the application.
    https://gitlab.com/littlesaints/functional-streams#-trial

- Insights

    Once you have modularized your application and Protean is wiring it together based on your configuration, 
    it can provide insights on how each section of your application flow is performing. 
    These metrics can be easily connected with any analytics software to visualize these insights about your application.

- Clean Shutdown

    The error rate for any service spikes when it's shutting down. 
    It's desirable to avoid it during controlled shutdown e.g. a new release.
    Protean allows you to configure clean shutdown (configuration only), where the application can be signalled for a shutdown.
    The framework stops the incoming messages / requests and blocks the call until the processing is complete.

- Event Caching

    Protean is an event driven framework. An event (yours or the default) passes through all the stages of your application flow.
    Protean provides for reusing these events, if you're concerned about creating new objects. 
    Infrequent Minor collections are usually not a performance issue but if they are for your application, 
    then event caching can be configured.

## Why should I use Protean ?

- Faster time to delivery of a robust and feature rich service / workflow, with lower cost and effort.

- Allow an application to choose / change an implementation based on it's functional and non-functional requirements.

- Save time and effort in building and testing controls and features, other than the application logic.

- Save time in building and testing the plumbing code.

- Although recommended but avoid the necessity of in-depth knowledge of a framework's API / DSL, to be able to implement it.

By the time we kick-off the design, we are (almost always) already locked in with a specific framework. 
With time, we see enhancement to the business logic coming in with a short deadline (usually).
Sometimes, we need to make subtle changes to what we already have but for a different project, which has a different deployment strategy or non-functional requirement.
All these changes, require changes (DSL or not) in code which increase the delivery time and cost.

At times our latency / throughput requirements change because of our business needs and we need our services to perform better.
This requires performance analysis and controls in the short run and changes in the long run, to ensure the service performs as per expectation and without much effort.

Protean provides some useful features like *easy* scaling up, size and memory throttles, granular performance statistics, clean shutdown, caching, retry-able operations and few others, that are not available in most frameworks. 
This ensures that you don't put effort in building and / or incorporating them in your application and spend time building only your business logic.
This fast tracks delivery and reduces the overall cost of developing and testing an application.

Protean also promotes modular application design.
The application benefits from building it's code as reusable and configurable *functions() that can be easily **unit** tested and configured in a workflow, thereby boosting your code coverage and reliability.

### Documentation

See the [Wiki][wiki] for full documentation, examples, operational details and other information.

[What Java 'type' to choose for a stage's processor ?](https://gitlab.com/littlesaints/workflow/protean-suite/protean/wikis/faq#what-java-type-to-choose-for-a-stage-processor-)

### Code Samples

Code sample and a brief tutorial is available [here](https://gitlab.com/littlesaints/workflow/protean-suite/protean/-/wikis/code).

You can also checkout the [test-suite](https://gitlab.com/littlesaints/workflow/protean-suite/protean/tree/master/protean-tests) for examples on how to configure the various features.  

### Usage
Download the latest version
- protean-core: [MVN Repository][latest-release-core], [Maven Central][maven-central_core] 
- protean-spring: [MVN Repository][latest-release-spring], [Maven Central][maven-central_spring] 

or depend via:

**Gradle**
```gradle
compile 'io.gitlab.littlesaints:protean-core:1.0.0'
```
or
```gradle
compile 'io.gitlab.littlesaints:protean-spring:1.0.0'
```


**Maven**
```maven
<dependency>
    <groupId>io.gitlab.littlesaints</groupId>
    <artifactId>protean-core</artifactId>
    <version>1.0.0</version>
</dependency>
```
or
```maven
<dependency>
    <groupId>io.gitlab.littlesaints</groupId>
    <artifactId>protean-spring</artifactId>
    <version>1.0.0</version>
</dependency>
```

Snapshot versions are available in [Sonatype's snapshots repository (protean-core)][snapshots-core] and [Sonatype's snapshots repository (protean-spring)][snapshots-spring].

## Maintainer
[@varunanandrajput](https://gitlab.com/varunanandrajput)

### License
[GNU Lesser General Public License v3](lgplv3)

Copyright (C) 2018 Varun Anand

[api_badge]: https://img.shields.io/badge/docs-API-orange.svg    
[api]: https://littlesaints.gitlab.io/workflow/protean-suite/protean/api
[wiki_badge]: https://img.shields.io/badge/docs-WIKI-orange.svg
[wiki]: https://gitlab.com/littlesaints/workflow/protean-suite/protean/wikis/home
[maven-central_badge]: https://maven-badges.herokuapp.com/maven-central/io.gitlab.littlesaints/protean-core/badge.svg
[maven-central_core]: https://search.maven.org/artifact/io.gitlab.littlesaints/protean-core
[latest-release-core]: https://mvnrepository.com/artifact/io.gitlab.littlesaints/protean-core/latest
[snapshots-core]: https://oss.sonatype.org/content/repositories/snapshots/io/gitlab/littlesaints/protean-core
[maven-central_spring]: https://search.maven.org/artifact/io.gitlab.littlesaints/protean-spring
[latest-release-spring]: https://mvnrepository.com/artifact/io.gitlab.littlesaints/protean-spring/latest
[snapshots-spring]: https://oss.sonatype.org/content/repositories/snapshots/io/gitlab/littlesaints/protean-spring
[build_badge]: https://gitlab.com/littlesaints/workflow/protean-suite/protean/badges/master/pipeline.svg
[build]: https://gitlab.com/littlesaints/workflow/protean-suite/protean/pipelines
[coverage_badge]: https://gitlab.com/littlesaints/workflow/protean-suite/protean/badges/master/coverage.svg?job=build
[coverage]: https://littlesaints.gitlab.io/workflow/protean-suite/protean/tests/coverage
[tests_badge]: https://img.shields.io/badge/report-tests-neon.svg
[tests]: https://littlesaints.gitlab.io/workflow/protean-suite/protean/tests/html
[license_badge]: https://img.shields.io/badge/license-LGPLv3-blue.svg
[lgplv3]: https://www.gnu.org/licenses/lgpl-3.0.en.html