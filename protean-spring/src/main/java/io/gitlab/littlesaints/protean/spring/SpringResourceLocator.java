/*
 *                     protean-spring
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-spring.
 *
 * protean-spring is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-spring is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.spring;

import io.gitlab.littlesaints.protean.resources.ResourceLocator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Optional;

/**
 * @author Varun Anand
 * @since 1.0
 */
@Service
public class SpringResourceLocator implements ResourceLocator {

	public static String TYPE = "SPRING";

	@Autowired
	private ApplicationContext applicationContext;

	@PostConstruct
	private void init() {
		REGISTRY.register(TYPE, this);
	}

	@Override
	public <T> Optional<T> doLookup(String name) {
		return Optional.ofNullable((T)applicationContext.getBean(name));
	}
}
