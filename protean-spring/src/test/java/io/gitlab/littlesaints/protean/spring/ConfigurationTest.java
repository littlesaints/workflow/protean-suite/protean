/*
 *                     protean-spring
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-spring.
 *
 * protean-spring is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-spring is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.spring;

import io.gitlab.littlesaints.protean.Context;
import io.gitlab.littlesaints.protean.configuration.Configuration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.atomic.LongAdder;
import java.util.function.Function;
import java.util.function.IntSupplier;
import java.util.stream.IntStream;

/**
 * @author Varun Anand
 * @since 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Configurer.class})
public class ConfigurationTest {

	private static final IntSupplier LOAD_FACTOR_RESOLVER = () -> (Integer) System.getProperties().getOrDefault("test-load-factor", 1);

	@Autowired
	private Configuration configuration;

	@Autowired
	private LongAdder acks;

	@Test
	public void test() throws Exception {
		final Context<Integer, ?> testContext = configuration.<Integer, Object>getContext("testContext")
				.orElseThrow(IllegalAccessException::new);
		final Function<Integer, ?> endpoint = testContext.getEndpoint();

		final int load = 1;
		acks.reset();
		IntStream.range(0, load).forEach(endpoint::apply);
		while (acks.sum() != load) {
			Thread.yield();
		}
	}

	//TODO eventCache testing

	@Test
	public void perfTest() throws Exception {
		final Context<Integer, ?> testContext = configuration.<Integer, Object>getContext("perfTestContext")
				.orElseThrow(IllegalAccessException::new);
		final Function<Integer, ?> endpoint = testContext.getEndpoint();

		final int load = 1_000 * LOAD_FACTOR_RESOLVER.getAsInt();
		acks.reset();
		IntStream.range(0, load).forEach(i -> endpoint.apply(1));
		while (acks.sum() != load) {
			Thread.yield();
		}
	}

}
