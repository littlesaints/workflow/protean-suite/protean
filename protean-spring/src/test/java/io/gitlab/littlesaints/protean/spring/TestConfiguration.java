/*
 *                     protean-spring
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-spring.
 *
 * protean-spring is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-spring is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.spring;

import io.gitlab.littlesaints.protean.analytics.AckListener;
import io.gitlab.littlesaints.protean.analytics.StatsListener;
import io.gitlab.littlesaints.protean.configuration.Configuration;
import io.gitlab.littlesaints.protean.event.SimpleEvent;
import io.gitlab.littlesaints.protean.event.State;
import org.springframework.context.annotation.Bean;

import java.util.concurrent.atomic.LongAdder;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author Varun Anand
 * @since 1.0
 */
@org.springframework.context.annotation.Configuration
public class TestConfiguration {

    @Bean
    public Supplier<Function<SimpleEvent, State>> filter() {
        return () -> e -> State.SUCCESS;
    }

    @Bean
    public Supplier<Function<SimpleEvent, State>> transformer() {
        return () -> e -> State.SUCCESS;
    }

    @Bean
    public Supplier<Function<SimpleEvent, State>> calculator() {
        return () -> e -> State.SUCCESS;
    }

    @Bean
    public Supplier<StatsListener<SimpleEvent>> logger() {
        return () -> (cx, e, n, ss, se) -> System.out.println(e.getPayload() + "--");
    }

    @Bean
    public Supplier<AckListener<SimpleEvent>> noopAckListener() {
        return () -> (cx, e, ss, se) -> {
        };
    }

    @Bean
    public LongAdder acks() {
        return new LongAdder();
    }

    @Bean
    public Supplier<AckListener<SimpleEvent>> statsListener() {
        final LongAdder acks = acks();
        return () -> (cx, e, ss, se) -> acks.increment();
    }

    @Bean
    public Configuration configuration() throws Exception {
//		return Configuration.loadYaml(Configuration.Source.FILE, "C:\\Users\\vanand2\\home\\workspaces\\sandbox\\protean-spring\\src\\test\\resources\\testConfiguration.yaml");
        return Configuration.loadJsonFile("C:\\himani_varun\\varun\\workspaces\\Protean\\protean-spring\\src\\test\\resources\\testConfiguration.json");
    }
}
