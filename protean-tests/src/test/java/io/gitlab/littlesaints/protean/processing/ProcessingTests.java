/*
 *                     protean-tests
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-tests.
 *
 * protean-tests is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-tests is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.processing;

import io.gitlab.littlesaints.protean.Context;
import io.gitlab.littlesaints.protean.configuration.Configuration;
import io.gitlab.littlesaints.protean.configuration.ContextOptions;
import io.gitlab.littlesaints.protean.event.State;
import io.gitlab.littlesaints.protean.executors.sync.SyncExecutor;
//import io.gitlab.littlesaints.protean.ext.executors.disruptor.DisruptorExecutor;
//import io.gitlab.littlesaints.protean.ext.executors.streams.StreamsExecutor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.BiConsumer;
import java.util.function.IntConsumer;
import java.util.stream.IntStream;

@RunWith(Parameterized.class)
public class ProcessingTests {

    private final Configuration configuration;
    private final String executor;
    private boolean isMultiProducer;

    public ProcessingTests(String source, String executor, boolean isMultiProducer) throws Exception {
        final String confBasePath = "/processing/processingTestsContexts.";
        switch (source) {
            case "yaml":
                configuration = Configuration.loadYamlFileFromClasspath(ProcessingTests.class, confBasePath + source);
                break;
            case "json":
                configuration = Configuration.loadJsonFileFromClasspath(
                        ProcessingTests.class, confBasePath + source);
                break;
            default:
                configuration = null;
        }
        this.executor = executor;
        this.isMultiProducer = isMultiProducer;
    }

    @Parameterized.Parameters(name = "{index}: source={0} | executor={1} | isMultiProducer={2}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"json", SyncExecutor.TYPE, false},
                {"json", SyncExecutor.TYPE, true},
                {"yaml", SyncExecutor.TYPE, false},
                {"yaml", SyncExecutor.TYPE, true},
//            {"json", StreamsExecutor.TYPE, false},
//            {"json", StreamsExecutor.TYPE, true},
//            {"json", DisruptorExecutor.TYPE, false},
//            {"json", DisruptorExecutor.TYPE, true}
        });
    }

    @Before
    public void setup() {
        ProcessingTestsSupport.resetCounters();
    }

    /**
     * This tests ordered and unOrdered processing of events. It validate the following:
     * <ul>
     * <li>The messages are processed via the expected route/order.</li>
     * <li>The messages are processed followed the order as defined by route resolver.</li>
     * <li>All the messages are processed.</li>
     * <li>Expected number of processors are created.</li>
     * </ul>
     *
     * @throws Exception exception
     */
    @Test/*(timeout = 5_000L)*/
    public void validateOrderedProcessing() throws Exception {
        final int load = 1_000;
        Context<?, ?> context = publish(load, "OrderedProcessing");
        await(load);
        if (ProcessingTestsSupport.exceptionRef.get() != null && !SyncExecutor.TYPE.equals(executor)) {
            throw new AssertionError(ProcessingTestsSupport.exceptionRef.get());
        }
        Assert.assertEquals(load, ProcessingTestsSupport.acknowledgedByState.get(State.SUCCESS).sum());
        String[] stages = new String[]{"filtering", "transformation", "publish", "persist", "logging"};
        for (String stage : stages) {
            Assert.assertEquals(load, ProcessingTestsSupport.processedByStage.get(stage).sum());
        }
        Assert.assertEquals(4, ProcessingTestsSupport.routeByProcessingThread.size());
    }

    /**
     * This tests skipping of events. It validate the following:
     * <ul>
     * <li>The expected number of messages are skipped.</li>
     * <li>All the messages are processed.</li>
     * </ul>
     *
     * @throws Exception exception
     */
    @Test(timeout = 5_000L)
    public void validateSkippedProcessing() throws Exception {
        final int load = 1_000;
        Context<?, ?> context = publish(load, "SkippedProcessing");
        await(load);
        Assert.assertEquals(load / 10, ProcessingTestsSupport.acknowledgedByState.get(State.SUCCESS).sum());
        Assert.assertEquals(load / 10, ProcessingTestsSupport.processedByStage.get("transformation").sum());
        Assert.assertEquals(load, ProcessingTestsSupport.processedByStage.get("logging").sum());
        Assert.assertEquals(load - (load / 10), ProcessingTestsSupport.acknowledgedByState.get(State.SKIPPED).sum());
    }

    /**
     * This tests failing of events. It validate the following:
     * <ul>
     * <li>The expected number of messages have failed.</li>
     * <li>All the messages are processed.</li>
     * </ul>
     *
     * @throws Exception exception
     */
    @Test(timeout = 5_000L)
    public void validateFailedProcessing() throws Exception {
        final int load = 1_000;
        Context<?, ?> context = publish(load, "FailedProcessing");
        await(load);
        Assert.assertEquals(load / 10, ProcessingTestsSupport.acknowledgedByState.get(State.SUCCESS).sum());
        Assert.assertEquals(load / 10, ProcessingTestsSupport.processedByStage.get("transformation").sum());
        Assert.assertEquals(load, ProcessingTestsSupport.processedByStage.get("logging").sum());
        Assert.assertEquals(load - (load / 10), ProcessingTestsSupport.acknowledgedByState.get(State.FAILURE).sum());
    }

//	/**
//	 * This tests failing of events. It validate the following:
//	 * <ul>
//	 * <li>The expected number of messages have been retried.</li>
//	 * <li>All the messages are processed.</li>
//	 * </ul>
//	 *
//	 * @throws Exception exception
//	 */
//	@Test
//	public void validateRetryProcessing() throws Exception {
//		final int load = 10;
//		await(load, publish(load, "RetryProcessing"));
//		printEventStateCounts();
//		Assert.assertEquals(load, acknowledgedByState.get(State.SUCCESS).sum());
//		Assert.assertEquals((load / 2) - 1, retryCount.sum());
//	}

    /**
     * This tests failing of events. It validate the following:
     * <ul>
     * <li>The expected number of messages have been retried.</li>
     * <li>All the messages are processed.</li>
     * </ul>
     *
     * @throws Exception exception
     */
    @Test
    public void validateRetryProcessing() throws Exception {
        final int load = 10;
        Context<?, ?> context = publish(load, "RetryProcessing");
        await(load);
        Assert.assertEquals(load / 2, ProcessingTestsSupport.acknowledgedByState.get(State.SUCCESS).sum());
        Assert.assertEquals(load / 2, ProcessingTestsSupport.acknowledgedByState.get(State.FAILURE).sum());
        Assert.assertEquals((load * 3) / 2, ProcessingTestsSupport.totalRetries.sum());
    }

    /**
     * This tests failing of events. It validate the following:
     * <ul>
     * <li>The expected number of messages have been retried.</li>
     * <li>All the messages are processed.</li>
     * </ul>
     *
     * @throws Exception exception
     */
    @Test
    public void validateStandardProcessing() throws Exception {
        final int load = 1000;
        Context<?, ?> context = publish(load, "StandardProcessing");
        await(load);
        Assert.assertEquals(load - (load / 10), ProcessingTestsSupport.acknowledgedByState.get(State.SUCCESS).sum());
        Assert.assertEquals(load / 10, ProcessingTestsSupport.acknowledgedByState.get(State.FAILURE).sum());
        Assert.assertEquals(load, ProcessingTestsSupport.processedByStage.get("filtering").sum());
        Assert.assertEquals(load, ProcessingTestsSupport.processedByStage.get("transformation").sum());
        Assert.assertEquals(load - (load / 10), ProcessingTestsSupport.processedByStage.get("persist").sum());
        Assert.assertEquals(load, ProcessingTestsSupport.processedByStage.get("publish").sum());
        Assert.assertEquals(load - (load / 10), ProcessingTestsSupport.processedByStage.get("reporting").sum());
        Assert.assertEquals(load, ProcessingTestsSupport.processedByStage.get("logging").sum());
    }

    @Test
    public void validateStandardSyncProcessing() throws Exception {
        final int load = 1000;
        LongAdder counter = new LongAdder();
        Context<Integer, Double> context = publish(load, "StandardSyncProcessing",
                (i, r) -> {
                    Assert.assertEquals(Math.pow(i, 2), r, 0);
                    counter.increment();
                });
        await(load);
        Assert.assertEquals(load, counter.sum());
        Assert.assertEquals(load - (load / 10), ProcessingTestsSupport.acknowledgedByState.get(State.SUCCESS).sum());
        Assert.assertEquals(load / 10, ProcessingTestsSupport.acknowledgedByState.get(State.FAILURE).sum());
        Assert.assertEquals(load, ProcessingTestsSupport.processedByStage.get("filtering").sum());
        Assert.assertEquals(load, ProcessingTestsSupport.processedByStage.get("transformation").sum());
        Assert.assertEquals(load - (load / 10), ProcessingTestsSupport.processedByStage.get("persist").sum());
        Assert.assertEquals(load, ProcessingTestsSupport.processedByStage.get("publish").sum());
        Assert.assertEquals(load - (load / 10), ProcessingTestsSupport.processedByStage.get("reporting").sum());
        Assert.assertEquals(load, ProcessingTestsSupport.processedByStage.get("logging").sum());
    }

    /**
     * This tests failing of events. It validate the following:
     * <ul>
     * <li>The expected number of messages have been retried.</li>
     * <li>All the messages are processed.</li>
     * </ul>
     *
     * @throws Exception exception
     */
    @Test
    public void validateExceptionHandledProcessing() throws Exception {
        final int load = 1000;
        Context<?, ?> context = publish(load, "ExceptionHandledProcessing");
        await(load);
        Assert.assertEquals(load, ProcessingTestsSupport.acknowledgedByState.get(State.SUCCESS).sum());
    }

    /**
     * This tests the clean shutdown of an executor. It validate the following:
     * <ul>
     * <li>The expected number of messages have been processed even after shutdown has been initiated.</li>
     * </ul>
     */
    @Test(timeout = 20_000L)
    public void validateCleanShutdown() throws Exception {
        final int load = 5;
        final Context<Integer, ?> context = publish(load, "CleanShutdown");
        Thread.sleep(1000);
        context.shutdown();
        await(load);
    }

    private <T> Context<Integer, T> publish(int load, String name) throws InterruptedException {
        return publish(load, name, (i, r) -> {
        });
    }

    private <T> Context<Integer, T> publish(int load, String name, BiConsumer<Integer, T> assertion) throws InterruptedException {
        final Context<Integer, T> context = getContext(name);
        IntConsumer action = i -> assertion.accept(i, context.getEndpoint().apply(i));
        if (isMultiProducer) {
            CountDownLatch latch = new CountDownLatch(2);
            new Thread(() -> {
                IntStream.range(0, load / 2).forEach(action);
                latch.countDown();
            }, "test-publisher-1").start();
            new Thread(() -> {
                IntStream.range(load / 2, load).forEach(action);
                latch.countDown();
            }, "test-publisher-2").start();
            latch.await();
        } else {
            IntStream.range(0, load).forEach(action);
        }
        return context;
    }

    private void await(int load) {
        while (ProcessingTestsSupport.totalAcknowledgements.sum() != load) {
            Thread.yield();
        }
    }

    private <T> Context<Integer, T> getContext(String name) {
        return configuration.<Integer, T>updateContext(name + "::" + this.executor, updateContextOptions(name))
                .orElseThrow(IllegalArgumentException::new);
    }

    private ContextOptions updateContextOptions(String name) {
        final ContextOptions options = configuration.getContextOptions(name).orElseThrow(IllegalArgumentException::new);
        options.getExecutor().getType().setSupplier(this.executor);
        options.getExecutor().getType().getConfig().put("ignoreNodeConcurrency", true);
        options.getExecutor().setMultiProducer(this.isMultiProducer);
        configuration.updateContext(name, options);
        return options;
    }

    @Test/*(timeout = 5_000L)*/
    public void validateInterconnectedProcessing() throws Exception {
        final int load = 1_000;
        final String contextName = "PartialProcessing";
        updateContextOptions(contextName);
        if (SyncExecutor.TYPE.equals(executor)) {
            final Optional<ContextOptions> optionsRef = configuration.getContextOptions(contextName);
            if (optionsRef.isPresent()) {
                final ContextOptions options = optionsRef.get();
                options.getAnalytics().getListeners().stream()
                        .filter(o -> "validateProcessingThread".equals(o.getSupplier()))
                        .forEach(o -> o.setSupplier("validateExecutingThread"));
                configuration.updateContext(contextName, options);
            } else {
                Assert.fail("Couldn't lookup Context Options !");
            }
        }
        Context<?, ?> context = publish(load, "InterconnectedProcessing");
        await(load);
        if (ProcessingTestsSupport.exceptionRef.get() != null) {
            throw new AssertionError(ProcessingTestsSupport.exceptionRef.get());
        }
        Assert.assertEquals(load, ProcessingTestsSupport.acknowledgedByState.get(State.SUCCESS).sum());
        String[] stages = new String[]{"filtering", "transformation", "publish", "persist", "logging"};
        for (String stage : stages) {
            Assert.assertEquals(load, ProcessingTestsSupport.processedByStage.get(stage).sum());
        }
        if (!SyncExecutor.TYPE.equals(executor)) {
            Assert.assertEquals(4, ProcessingTestsSupport.routeByProcessingThread.size());
        }
    }

//	/**
//	 * This tests size based throttle. It validate the following:
//	 * <ul>
//	 * <li>The expected number of messages have been processed newInstance no more than configured events been processed in parallel.</li>
//	 * </ul>
//	 */
//	@Test
//	public void validateSizeThrottle() {
//
//		final int load = 1_000_000;
//
//		final Context<Integer> context = getContext("ThrottledProcessing");
//		Consumer<Integer> endpoint = context.getEndpoint();
//
//		IntStream.range(0, load).forEach(endpoint::accept);
//		context.shutdown();
//		final Statistics statistics = context.getStatistics();
//		while (statistics.getAckCount() != load) {
//			Thread.yield();
//		}
//
//	}

    private void printEventStateCounts() {
        ProcessingTestsSupport.acknowledgedByState.forEach((k, v) -> System.out.println(k + " = " + v.sum()));
    }

}
