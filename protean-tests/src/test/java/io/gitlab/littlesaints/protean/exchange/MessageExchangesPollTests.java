/*
 *                     protean-tests
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-tests.
 *
 * protean-tests is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-tests is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.exchange;

import io.gitlab.littlesaints.protean.exchanges.MessageExchange;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

@RunWith(Parameterized.class)
public class MessageExchangesPollTests extends AbstractMessageExchangesTest {

    private int capacity;

    public MessageExchangesPollTests(String type, int capacity, int load, boolean isMultiProducer) {
        super(type, capacity, load, isMultiProducer);
        this.capacity = capacity < 0 ? 10_000 : capacity;
    }

    @Test
    public void testPoll() throws Exception {
        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
        final ForkJoinTask<Integer> readerTask = ForkJoinPool.commonPool().submit(() -> {
            Integer message;
            int totalRead = 0;
            for (int count = 0; count < load; Thread.yield(), count++, ++totalRead) {
                while ((message = exchangeReader.poll()) == null) {
                    Thread.yield();
                }
                if (count != message) {
                    throw new RuntimeException(new ComparisonFailure("Out of order processing.", String.valueOf(count), String.valueOf(message)));
                }
            }
            return totalRead;
        });

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, load).forEach(publisher::put);

        Assert.assertEquals(load, readerTask.get().intValue());
    }

    @Test
    public void testPollFailure() throws Exception {

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, 5).forEach(publisher::put);

        final ForkJoinTask<Integer> readerTask = ForkJoinPool.commonPool().submit(() -> {
            Integer message;
            int totalRead = 0;
            for (int count = 0; count < load; Thread.yield(), count++) {
                message = exchangeReader.poll();
                if (message != null) {
                    ++totalRead;
                }
            }
            return totalRead;
        });

        Assert.assertEquals(5, readerTask.get().intValue());
    }

    @Test
    public void testPollMinMaxAttempt() throws Exception {
        final int min = 10;
        final int max = Math.min(10_000, Math.min(load - 1, capacity));
        final int attempts = 1;

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, max).forEach(publisher::put);

        ForkJoinTask<Integer> readerTask = ForkJoinPool.commonPool().submit(() -> {
            List<Integer> messages;
            int totalRead = 0;
            for (int count = 0; count < load; totalRead += messages.size()) {
                for (; ; Thread.yield()) {
                    messages = exchangeReader.poll(min, max, attempts);
                    if (!messages.isEmpty()) {
                        break;
                    }
                }
                if (messages.size() > max) {
                    throw new RuntimeException("Out of polling range.");
                }
                for (int message : messages) {
                    if (count++ != message) {
                        throw new RuntimeException(new ComparisonFailure("Out of order processing.", String.valueOf(count), String.valueOf(message)));
                    }
                }
            }
            return totalRead;
        });

        IntStream.range(max, load).forEach(publisher::put);

        Assert.assertEquals(load, readerTask.get().intValue());
    }

    @Test
    public void testPollMinMaxAttemptsFailure() throws Exception {
        final int min = 10;
        final int max = 10_000;
        final int attempts = 1000;

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, 5).forEach(publisher::put);

        ForkJoinTask<Integer> readerTask = ForkJoinPool.commonPool().submit(() -> {
            int totalRead = 0;
            totalRead += exchangeReader.poll(min, max, attempts).size();
            return totalRead;
        });

        Assert.assertEquals(5, readerTask.get().intValue());
    }

    @Test
    public void testPollMinMaxAttemptsAccuracy() throws Exception {
        final int min = 2;
        final int max = 4;
        final int maxAttempts = 2;

        final int[] maxBatchRead = new int[1];
        final int[] totalRead = new int[1];

        final BlockingQueue<String> goRead = new ArrayBlockingQueue<>(1);
        final BlockingQueue<String> goWrite = new ArrayBlockingQueue<>(1);

        final AtomicInteger expectedBatchSize = new AtomicInteger();
        final AtomicBoolean continueTest = new AtomicBoolean(true);

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
        ForkJoinTask<?> readerTask = ForkJoinPool.commonPool().submit(() -> {
            List<Integer> messages;
            try {
                for (int count = 0; count < load; Thread.yield()) {
                    for (; ; Thread.yield()) {
                        goRead.take();
                        messages = exchangeReader.poll(min, max, maxAttempts);
                        goWrite.put("");
                        if (!messages.isEmpty()) {
                            break;
                        }
                    }
                    totalRead[0] += messages.size();
                    maxBatchRead[0] = Math.max(maxBatchRead[0], messages.size());
                    if (messages.size() > expectedBatchSize.get()) {
                        continueTest.set(false);
                        goWrite.put("");
                        goRead.take();
                        throw new RuntimeException(new ComparisonFailure("Invalid batch size.", String.valueOf(expectedBatchSize.get()), String.valueOf(messages.size())));
                    }
                    for (int message : messages) {
                        if (count != message) {
                            continueTest.set(false);
                            goWrite.put("");
                            goRead.take();
                            throw new RuntimeException(new ComparisonFailure("Out of order processing.", String.valueOf(count), String.valueOf(message)));
                        }
                        ++count;
                    }
                }
            } catch (Exception x) {
                Assert.fail(x.getCause().getMessage());
            }
        });

        final int startOfBatch8 = load - (((load / 2) / 8) * 8);
        final int startOfBatch4 = startOfBatch8 - (((startOfBatch8 / 2) / 4) * 4);
        final int startOfBatch2 = startOfBatch4 / 2;

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();

        goWrite.put("");

        expectedBatchSize.set(1);
        for (int i = 0; i < startOfBatch2 && continueTest.get(); ) {
            goWrite.take();
            publisher.put(i++);
            goRead.put("");
        }
        Assert.assertEquals(1, maxBatchRead[0]);
//		Assert.assertEquals(load / 4, totalRead[0]);

        expectedBatchSize.set(2);
        for (int i = startOfBatch2; i < startOfBatch4 && continueTest.get(); ) {
            goWrite.take();
            publisher.put(i++);
            publisher.put(i++);
            goRead.put("");
        }
        Assert.assertEquals(2, maxBatchRead[0]);
//		Assert.assertEquals(load / 2, totalRead[0]);

        expectedBatchSize.set(4);
        for (int i = startOfBatch4; i < startOfBatch8 && continueTest.get(); ) {
            goWrite.take();
            publisher.put(i++);
            publisher.put(i++);
            publisher.put(i++);
            publisher.put(i++);
            goRead.put("");
        }
        Assert.assertEquals(4, maxBatchRead[0]);
//		Assert.assertEquals((3 * load) / 4, totalRead[0]);

        expectedBatchSize.set(4);
        for (int i = startOfBatch8; i < load && continueTest.get(); ) {
            goWrite.take();
            publisher.put(i++);
            publisher.put(i++);
            publisher.put(i++);
            publisher.put(i++);
            publisher.put(i++);
            publisher.put(i++);
            publisher.put(i++);
            publisher.put(i++);
            goRead.put("");
            goWrite.take();
            goRead.put("");
        }

        readerTask.get();
        Assert.assertEquals(load, totalRead[0]);
        Assert.assertEquals(4, maxBatchRead[0]);
    }

    @Test
    public void testPollMaxAttempts() throws Exception {
        final int max = Math.min(10_000, Math.min(load - 1, capacity));
        final int attempts = 10;

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, max).forEach(publisher::put);

        ForkJoinTask<Integer> readerTask = ForkJoinPool.commonPool().submit(() -> {
            List<Integer> messages;
            int totalRead = 0;
            for (int count = 0; count < load; totalRead += messages.size()) {
                for (; ; Thread.yield()) {
                    messages = exchangeReader.poll(max, attempts);
                    if (!messages.isEmpty()) {
                        break;
                    }
                }
                if (messages.size() > max) {
                    throw new RuntimeException("Out of polling range.");
                }
                for (int message : messages) {
                    if (count++ != message) {
                        throw new RuntimeException(new ComparisonFailure("Out of order processing.", String.valueOf(count), String.valueOf(message)));
                    }
                }
            }
            return totalRead;
        });

        IntStream.range(max, load).forEach(publisher::put);

        Assert.assertEquals(load, readerTask.get().intValue());
    }

    @Test
    public void testPollMaxAttemptsFailure() throws Exception {
        final int max = 10_000;
        final int attempts = 1000;

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, 5).forEach(publisher::put);

        ForkJoinTask<Integer> readerTask = ForkJoinPool.commonPool().submit(() -> {
            int totalRead = 0;
            totalRead += exchangeReader.poll(max, attempts).size();
            return totalRead;
        });

        Assert.assertEquals(5, readerTask.get().intValue());
    }

    @Test
    public void testPollMax() throws Exception {
        final int max = Math.min(10_000, Math.min(load - 1, capacity));

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, max).forEach(publisher::put);

        ForkJoinTask<Integer> readerTask = ForkJoinPool.commonPool().submit(() -> {
            List<Integer> messages;
            int totalRead = 0;
            for (int count = 0; count < load; totalRead += messages.size()) {
                for (; ; Thread.yield()) {
                    messages = exchangeReader.poll(max);
                    if (!messages.isEmpty()) {
                        break;
                    }
                }
                if (messages.size() > max) {
                    throw new RuntimeException("Out of polling range.");
                }
                for (int message : messages) {
                    if (count != message) {
                        throw new RuntimeException(new ComparisonFailure("Out of order processing.", String.valueOf(count), String.valueOf(message)));
                    }
                    ++count;
                }
            }
            return totalRead;
        });

        IntStream.range(max, load).forEach(publisher::put);

        Assert.assertEquals(load, readerTask.get().intValue());
    }

    @Test
    public void testPollMaxFailure() throws Exception {
        final int max = Math.min(10_000, capacity);

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, 5).forEach(publisher::put);

        ForkJoinTask<Integer> readerTask = ForkJoinPool.commonPool().submit(() -> {
            int totalRead = 0;
            totalRead += exchangeReader.poll(max).size();
            return totalRead;
        });

        Assert.assertEquals(5, readerTask.get().intValue());
    }

    @Test
    public void testPollAttemptsConsumer() throws Exception {
        final int attempts = 10;
        final boolean[] result = new boolean[1];

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
        ForkJoinTask<Integer> readerTask = ForkJoinPool.commonPool().submit(() -> {
            int[] totalRead = new int[1];
            for (; totalRead[0] != load; Thread.yield()) {
                result[0] = exchangeReader.poll(attempts, i -> totalRead[0]++ == i && totalRead[0] != load);
            }
            return totalRead[0];
        });

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, load).forEach(publisher::put);

        Assert.assertEquals(load, readerTask.get().intValue());
        Assert.assertFalse(result[0]);
    }

    @Test
    public void testPollAttemptsConsumerFailure() throws Exception {
        final int attempts = 10;
        final boolean[] result = new boolean[1];

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, 5).forEach(publisher::put);

        ForkJoinTask<Integer> readerTask = ForkJoinPool.commonPool().submit(
                () -> {
                    int[] totalRead = new int[1];
                    result[0] = exchangeReader.poll(attempts, i -> totalRead[0]++ == i && totalRead[0] != load);
                    return totalRead[0];
                });

        Assert.assertEquals(5, readerTask.get().intValue());
        Assert.assertTrue(result[0]);
    }

    @Test
    public void testPollAttemptsConsumers() throws Exception {
        final int attempts = 10;
        final int[] totalRead = new int[2];

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
        ForkJoinTask<Boolean> readerTask = ForkJoinPool.commonPool().submit(() -> {
            boolean result = true;
            for (; totalRead[0] != load; Thread.yield()) {
                result = exchangeReader.poll(attempts,
                        i -> totalRead[0]++ == i && totalRead[0] != load,
                        i -> totalRead[1]++ == i && totalRead[1] != load);
            }
            return result;
        });

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, load).forEach(publisher::put);

        Assert.assertFalse(readerTask.get());
        Assert.assertEquals(load, totalRead[0]);
        Assert.assertEquals(load, totalRead[1]);
    }

    @Test
    public void testPollAttemptsConsumersFailure() throws Exception {
        final int maxAttempts = 10;
        final int[] totalRead = new int[2];

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, 5).forEach(publisher::put);

        ForkJoinTask<Boolean> readerTask = ForkJoinPool.commonPool().submit(() ->
            exchangeReader.poll(maxAttempts,
                    i -> totalRead[0]++ == i && totalRead[0] != load,
                    i -> totalRead[1]++ == i && totalRead[1] != 3)
        );

        Assert.assertTrue(readerTask.get());
        Assert.assertEquals(5, totalRead[0]);
        Assert.assertEquals(3, totalRead[1]);
    }

    @Test
    public void testPollMinMaxAttemptsConsumer() throws Exception {
        final int min = 10;
        final int max = 10_000;
        final int attempts = 10;
        final int[] totalRead = new int[1];

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
        ForkJoinTask<Boolean> readerTask = ForkJoinPool.commonPool().submit(() -> {
            boolean result = false;
            for (; totalRead[0] != load; Thread.yield()) {
                result = exchangeReader.poll(min, max, attempts, b -> {
                    totalRead[0] += b.size();
                    return totalRead[0] != load && b.size() <= max;
                });
            }
            return result;
        });

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, load).forEach(publisher::put);

        Assert.assertTrue(readerTask.get());
        Assert.assertEquals(load, totalRead[0]);
    }

    @Test
    public void testPollMinMaxAttemptsConsumerFailure() throws Exception {
        final int min = 2;
        final int max = 10;
        final int attempts = 10;
        final int[] totalRead = new int[1];

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, capacity).forEach(publisher::put);

        ForkJoinTask<Boolean> readerTask = ForkJoinPool.commonPool().submit(() ->
                 exchangeReader.poll(min, max, attempts, b -> {
                    totalRead[0] += b.size();
                    return totalRead[0] != max*2 && b.size() <= max;
                })
        );

        Assert.assertTrue(readerTask.get());
        Assert.assertEquals(max*2, totalRead[0]);
    }

    @Test
    public void testPollMinMaxAttemptsConsumers() throws Exception {
        final int min = 10;
        final int max = 100;
        final int attempts = 10;
        final int[] totalRead = new int[2];

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
        ForkJoinTask<Boolean> readerTask = ForkJoinPool.commonPool().submit(() -> {
            boolean result = false;
            for (; totalRead[0] != load; Thread.yield()) {
                result = exchangeReader.poll(min, max, attempts,
                        b -> {
                            totalRead[0] += b.size();
                            return totalRead[0] != load && b.size() <= max;
                        },
                        b -> {
                            totalRead[1] += b.size();
                            return totalRead[1] != load && b.size() <= max;
                        });
            }
            return result;
        });

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, load).forEach(publisher::put);

        Assert.assertTrue(readerTask.get());
        Assert.assertEquals(load, totalRead[0]);
        Assert.assertEquals(load, totalRead[1]);
    }

    @Test
    public void testPollMinMaxAttemptsConsumersFailure() throws Exception {
        final int min = 2;
        final int max = 10;
        final int attempts = 10;
        final int[] totalRead = new int[2];

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, capacity).forEach(publisher::put);

        ForkJoinTask<Boolean> readerTask = ForkJoinPool.commonPool().submit(() ->
            exchangeReader.poll(min, max, attempts,
                b -> {
                    totalRead[0] += b.size();
                    return totalRead[0] != max * 2;
                },
                b -> {
                    totalRead[1] += b.size();
                    return false;
                }));

        Assert.assertTrue(readerTask.get());
        Assert.assertEquals(max * 2, totalRead[0]);
        Assert.assertEquals(max, totalRead[1]);
    }

    @Test
    public void testPollMaxAttemptsConsumer() throws Exception {
        final int max = 10_000;
        final int attempts = 10;
        final int[] totalRead = new int[1];

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
        ForkJoinTask<Boolean> readerTask = ForkJoinPool.commonPool().submit(() -> {
            boolean result = false;
            for (; totalRead[0] != load; Thread.yield()) {
                result = exchangeReader.poll(max, attempts, b -> {
                    totalRead[0] += b.size();
                    return totalRead[0] != load && b.size() <= max;
                });
            }
            return result;
        });

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, load).forEach(publisher::put);

        Assert.assertTrue(readerTask.get());
        Assert.assertEquals(load, totalRead[0]);
    }

    @Test
    public void testPollMaxAttemptsConsumerFailure() throws Exception {
        final int max = capacity - 5;
        final int attempts = 10;
        final int[] totalRead = new int[1];

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, capacity).forEach(publisher::put);

        ForkJoinTask<Boolean> readerTask = ForkJoinPool.commonPool().submit(() ->
                exchangeReader.poll(max, attempts, b -> {
                    totalRead[0] += b.size();
                    return totalRead[0] != load && b.size() <= max;
                })
        );

        Assert.assertFalse(readerTask.get());
        Assert.assertEquals(capacity, totalRead[0]);
    }

    @Test
    public void testPollMaxAttemptsConsumers() throws Exception {
        final int max = 10_000;
        final int attempts = 10;
        final int[] totalRead = new int[2];

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
        ForkJoinTask<Boolean> readerTask = ForkJoinPool.commonPool().submit(() -> {
            boolean result = false;
            for (; totalRead[0] != load; Thread.yield()) {
                result = exchangeReader.poll(max, attempts,
                        b -> {
                            totalRead[0] += b.size();
                            return totalRead[0] != load && b.size() <= max;
                        },
                        b -> {
                            totalRead[1] += b.size();
                            return totalRead[1] != load && b.size() <= max;
                        });
            }
            return result;
        });

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, load).forEach(publisher::put);

        Assert.assertTrue(readerTask.get());
        Assert.assertEquals(load, totalRead[0]);
        Assert.assertEquals(load, totalRead[1]);
    }

    @Test
    public void testPollMaxAttemptsConsumersFailure() throws Exception {
        final int max = capacity - 5;
        final int attempts = 10;
        final int[] totalRead = new int[2];

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, capacity).forEach(publisher::put);

        ForkJoinTask<Boolean> readerTask = ForkJoinPool.commonPool().submit(() ->
                exchangeReader.poll(max, attempts,
                        b -> {
                            totalRead[0] += b.size();
                            return totalRead[0] != load && b.size() <= max;
                        },
                        b -> {
                            totalRead[1] += b.size();
                            return totalRead[1] != load && b.size() <= max && totalRead[1] != max;
                        })
        );

        Assert.assertFalse(readerTask.get());
        Assert.assertEquals(capacity, totalRead[0]);
        Assert.assertEquals(max, totalRead[1]);
    }

    @Test
    public void testBatchPollPerformance() throws Exception {

        final int[] totalRead = new int[1];

        final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
        ForkJoinTask<?> readerTask = ForkJoinPool.commonPool().submit(() -> {
            List<Integer> messages;
            for (int count = 0; count < load; totalRead[0] += messages.size()) {
                for (; ; Thread.yield()) {
                    messages = exchangeReader.poll(1, 100_000, 10);
                    if (!messages.isEmpty()) {
                        break;
                    }
                }
                for (int message : messages) {
                    if (count++ != message) {
                        throw new RuntimeException(new ComparisonFailure("Out of order processing.", String.valueOf(count), String.valueOf(message)));
                    }
                }

            }
        });

        final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
        IntStream.range(0, load).forEach(publisher::put);

        readerTask.get();
        Assert.assertEquals(load, totalRead[0]);
    }

    @Test
    public void testBatchPollMultiProducersPerformance() throws Exception {

        if (isMultiProducer) {

            final int[] totalRead = new int[1];

            final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
            ForkJoinTask<?> readerTask = ForkJoinPool.commonPool().submit(() -> {
                List<Integer> messages;
                for (; totalRead[0] < load; totalRead[0] += messages.size()) {
                    for (; ; Thread.yield()) {
                        messages = exchangeReader.poll(1, 100_000, 10);
                        if (!messages.isEmpty()) {
                            break;
                        }
                    }
                }
            });

            final MessageExchange.Writer<Integer> publisher1 = exchange.newWriter();
            ForkJoinTask<?> publisher1Task = ForkJoinPool.commonPool().submit(() ->
                    IntStream.range(0, load / 2).forEach(publisher1::put));

            final MessageExchange.Writer<Integer> publisher2 = exchange.newWriter();
            ForkJoinTask<?> publisher2Task = ForkJoinPool.commonPool().submit(() ->
                    IntStream.range(load / 2, load).forEach(publisher2::put));

            publisher1Task.get();
            publisher2Task.get();
            readerTask.get();
            Assert.assertEquals(load, totalRead[0]);
        }
    }

}