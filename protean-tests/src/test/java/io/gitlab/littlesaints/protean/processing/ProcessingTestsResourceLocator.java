/*
 *                     protean-tests
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-tests.
 *
 * protean-tests is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-tests is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.processing;

import io.gitlab.littlesaints.protean.analytics.AckListener;
import io.gitlab.littlesaints.protean.configuration.Config;
import io.gitlab.littlesaints.protean.configuration.Props;
import io.gitlab.littlesaints.protean.event.SimpleEvent;
import io.gitlab.littlesaints.protean.event.State;
import io.gitlab.littlesaints.protean.resources.Registrar;
import io.gitlab.littlesaints.protean.resources.ResourceLocator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;

public class ProcessingTestsResourceLocator implements ResourceLocator, ProcessingTestsSupport {

	public static class _Registrar implements Registrar {
		static {
			REGISTRY.register("PROCESSING_TESTS", new ProcessingTestsResourceLocator());
		}
	}

	private ProcessingTestsResourceLocator() {}

	private final Props resources = new Props();

	private final Function<Config, Function<SimpleEvent, State>> registerNodeProcessing = c -> {
		final String nodeName = c.get("node");
		processedByStage.putIfAbsent(nodeName, new LongAdder());
		return e -> {
			processedByStage.get(nodeName).increment();
			return e.getState();
		};
	};

	private final BiFunction<Config, Map<String, Function<Integer, ?>>, Function<SimpleEvent, State>> nodeContextProcessingCounter = (c, m) -> {
		final String nodeName = c.get("node");
		processedByStage.putIfAbsent(nodeName, new LongAdder());
		return e -> {
			processedByStage.get(nodeName).increment();
			m.get("PartialProcessing").apply(e.getPayload());
			return e.getState();
		};
	};

	private final Function<Config, Function<SimpleEvent, State>> addProcessingThreadId = c -> {
		final String key = c.get("key");
		return e -> {
			e.setProp(key, Thread.currentThread().getId());
			return State.SUCCESS;
		};
	};

	private final Function<Config, Function<SimpleEvent, State>> moduloStateProcessor = c -> {
		final int factor = c.get("modulo");
		final State moduloState = State.valueOf(c.get("moduloState"));
		final State nonModuloState = State.valueOf(c.get("nonModuloState"));
		return e -> e.<Integer>getPayload() % factor == 0 ? moduloState : nonModuloState;
	};

	private final Function<Config, Function<SimpleEvent, State>> moduloExceptionThrower = c -> {
		final int factor = c.get("modulo");
		final State moduloState = State.valueOf(c.get("moduloState"));
		return e -> {
			if (e.<Integer>getPayload() % factor == 0) {
				return moduloState;
			} else {
				throw new RuntimeException();
			}
		};
	};

	private final Function<Integer, ToIntFunction<SimpleEvent>> modulo4Router = factor -> e -> e.<Integer>getPayload() % factor;

	private final Supplier<AckListener<SimpleEvent>> registerAcknowledgedState = () ->
			(cx, e, es, ee) -> acknowledgedByState.get(e.getState()).increment();

	private final Function<Config, AckListener<SimpleEvent>> validateProcessingThread = c -> {
		final String key = c.get("key");
		final ToIntFunction<SimpleEvent> routeResolver = modulo4Router.apply(c.get("modulo"));
		return (cx, e, es, ee) -> {
			Long processingThreadId = e.getProp(key);
			Long existingThreadId;
			if (null != (existingThreadId = routeByProcessingThread.putIfAbsent(routeResolver.applyAsInt(e), processingThreadId))) {
				if (!existingThreadId.equals(processingThreadId)) {
					exceptionRef.set(new RuntimeException("Unordered processing found !!"));
				}
			}
		};
	};

	private final Function<Config, AckListener<SimpleEvent>> validateExecutingThread = c -> {
		final String key = c.get("key");
		return (cx, e, es, ee) -> {
			Long processingThreadId = e.getProp(key);
			Long existingThreadId = Thread.currentThread().getId();
			if (!existingThreadId.equals(processingThreadId)) {
				exceptionRef.set(new RuntimeException("Unordered processing found !!"));
			}
		};
	};

	private final Function<Config, Function<SimpleEvent, State>> retryProcessor = c -> {
		final Integer factor = c.get("modulo");
		return e -> {
			Integer payload = e.getPayload();
			if (payload % factor == 0) {
				totalRetries.increment();
				return State.RETRY;
			}
			return State.SUCCESS;
		};
	};

	private final Function<Config, Function<SimpleEvent, State>> stageValidator = c -> {

		final String name = c.get("name");
		final List<String> expectedStages = c.get("expected");
		expectedStages.add(name);
		final Integer failDivisor = c.get("failDivisor");
		processedByStage.put(name, new LongAdder());

		return e -> {
			processedByStage.get(name).increment();
			ConcurrentSkipListSet<String> stagesProcessed = e.getProp("stagesProcessed");
			if (stagesProcessed == null) {
				stagesProcessed = new ConcurrentSkipListSet<>();
				e.setProp("stagesProcessed", stagesProcessed);
			}
			stagesProcessed.add(name);
			final ConcurrentSkipListSet<String> stagesProcessedToCompare = new ConcurrentSkipListSet<>(stagesProcessed);
			switch (name) {
				case "reporting":
					stagesProcessedToCompare.remove("persist");
					stagesProcessedToCompare.remove("logging");
					break;
				case "logging":
					stagesProcessedToCompare.remove("reporting");
				case "publish":
					stagesProcessedToCompare.remove("persist");
					break;
				case "persist":
					stagesProcessedToCompare.remove("publish");
					break;
			}

			State state = State.SUCCESS;
			if (expectedStages.size() != stagesProcessedToCompare.size() || !stagesProcessedToCompare.containsAll(expectedStages)) {
				state = State.FAILURE;
				e.setProp("stage", name);
				e.setProp("expectedStages", new ArrayList<>(expectedStages));
				e.setProp("stagesProcessedToCompare", new ArrayList<>(stagesProcessedToCompare));
			}
			int payload = e.<Integer>getPayload();
			if (failDivisor != null && payload % failDivisor == 0) {
				state = State.FAILURE;
				e.setProp("counter.get()", payload);
			}
			return state;
		};
	};

	private final Function<Config, Function<SimpleEvent, State>> barrier = c -> e -> {
		try {
			Thread.sleep(c.<Integer>get("waitInMillis"));
		} catch (InterruptedException ex) {
			throw new RuntimeException(ex);
		}
		return State.SUCCESS;
	};

	private final Function<SimpleEvent, Double> calcSquare = e -> Math.pow(e.<Integer>getPayload(), 2);

	private final Function<Config, BiFunction<SimpleEvent, Exception, State>> fixAllXHandler = c -> (e, x) -> State.SUCCESS;

	{
		resources.put("addProcessingThreadId", addProcessingThreadId);
		resources.put("modulo4Router", modulo4Router);
		resources.put("validateProcessingThread", validateProcessingThread);
		resources.put("validateExecutingThread", validateExecutingThread);
		resources.put("moduloStateProcessor", moduloStateProcessor);
		resources.put("registerAcknowledgedState", registerAcknowledgedState);
		resources.put("moduloExceptionThrower", moduloExceptionThrower);
		resources.put("retryProcessor", retryProcessor);
		resources.put("stageValidator", stageValidator);
		resources.put("fixAllXHandler", fixAllXHandler);
		resources.put("barrier", barrier);
		resources.put("registerNodeProcessing", registerNodeProcessing);
		resources.put("statsListener", statsListener);
		resources.put("nodeContextProcessingCounter", nodeContextProcessingCounter);
		resources.put("calcSquare", calcSquare);
	}

	@Override
	public <T> Optional<T> doLookup(String name) {
		return Optional.ofNullable(resources.get(name));
	}

}
