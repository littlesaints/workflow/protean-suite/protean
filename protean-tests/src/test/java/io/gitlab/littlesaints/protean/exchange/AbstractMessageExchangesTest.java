/*
 *                     protean-tests
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-tests.
 *
 * protean-tests is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-tests is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.exchange;

import io.gitlab.littlesaints.protean.configuration.Config;
import io.gitlab.littlesaints.protean.exchanges.MessageExchange;
import io.gitlab.littlesaints.protean.exchanges.blocking.LinkedBlockingExchange;
import io.gitlab.littlesaints.protean.exchanges.blocking.LinkedTransferExchange;
//import io.gitlab.littlesaints.protean.ext.exchanges.DisruptorExchange;
import io.gitlab.littlesaints.protean.resources.Registrar;
import lombok.extern.log4j.Log4j2;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Properties;
import java.util.ServiceLoader;

@RunWith(Parameterized.class)
@Log4j2
public abstract class AbstractMessageExchangesTest {

	final int load;
	final MessageExchange<Integer> exchange;
	final boolean isMultiProducer;

	static {
		ServiceLoader.load(Registrar.class).forEach(r -> log.info("Loading {}", r));
	}

	AbstractMessageExchangesTest(String type, int capacity, int load, boolean isMultiProducer) {
		this.load = load;
		this.isMultiProducer = isMultiProducer;
		this.exchange = MessageExchange.REGISTRY.lookupOrThrow(type, "exchange")
			.newInstance(capacity, isMultiProducer, new Config());
	}

	// name attribute is optional, provide an unique name for test
	// multiple parameters, uses Collection<Object[]>
	@Parameterized.Parameters(name = "{index}: getType={0} | capacity={1} | load={2} | isMultiProducer={3}")
	public static Collection<Object[]> data() {
		final Properties sysProps = System.getProperties();
		final int lf = (Integer) sysProps.getOrDefault("test-load-factor", 1);
		return Arrays.asList(new Object[][]{
			{LinkedBlockingExchange.TYPE, 32 * lf, 1_000 * lf, false},
			{LinkedBlockingExchange.TYPE, 32 * lf, 1_000 * lf, true},
			{LinkedTransferExchange.TYPE, -1, 1_000 * lf, false},
			{LinkedTransferExchange.TYPE, -1, 1_000 * lf, true},
//            {DisruptorExchange.TYPE, 32 * lf, 1_000 * lf, false},
//			{DisruptorExchange.TYPE, 32 * lf, 1_000 * lf, true}
//			{LinkedSyncTransferExchange.TYPE, -1, 1_000_000, false},
//			{LinkedSyncTransferExchange.TYPE, -1, 1_000_000, true}
		});
	}

}