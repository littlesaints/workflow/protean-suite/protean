/*
 *                     protean-tests
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-tests.
 *
 * protean-tests is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-tests is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.processing;

import io.gitlab.littlesaints.protean.analytics.AckListener;
import io.gitlab.littlesaints.protean.event.State;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.IntSupplier;
import java.util.function.Supplier;

/**
 * @author Varun Anand
 * @since 1.0
 */
public interface ProcessingTestsSupport {

	IntSupplier LOAD_FACTOR_RESOLVER = () -> (Integer) System.getProperties().getOrDefault("test-load-factor", 1);

	Map<State, LongAdder> acknowledgedByState = new ConcurrentHashMap<>();

	Map<Integer, Long> routeByProcessingThread = new ConcurrentHashMap<>();

	AtomicReference<Exception> exceptionRef = new AtomicReference<>();

	Map<String, LongAdder> processedByStage = new ConcurrentHashMap<>();

	LongAdder totalRetries = new LongAdder();

	LongAdder totalAcknowledgements = new LongAdder();

	Supplier<AckListener> statsListener = () ->
			(context, identifier, startMillis, endMillis) -> totalAcknowledgements.increment();

	static void resetCounters() {
		totalAcknowledgements.reset();
		if (acknowledgedByState.isEmpty()) {
			Arrays.stream(State.values()).forEach(s -> acknowledgedByState.put(s, new LongAdder()));
		} else {
			acknowledgedByState.values().forEach(LongAdder::reset);
		}
		routeByProcessingThread.clear();
		exceptionRef.set(null);
		totalRetries.reset();
		processedByStage.clear();
	}

}
