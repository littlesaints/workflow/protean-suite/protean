/*
 *                     protean-tests
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-tests.
 *
 * protean-tests is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-tests is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.exchange;

import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import io.gitlab.littlesaints.protean.exchanges.MessageExchange;

@RunWith(Parameterized.class)
public class MessageExchangesTakeTests extends AbstractMessageExchangesTest {

	public MessageExchangesTakeTests(String type, int capacity, int load, boolean isMultiProducer) {
		super(type, capacity, load, isMultiProducer);
	}

	//	@Test
	public void testTake() throws Exception {

		final int[] totalRead = new int[1];

		final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
		ForkJoinTask<?> readerTask = ForkJoinPool.commonPool().submit(() -> {
			Integer message;
			for (int count = 0; count < load; Thread.yield(), count++, ++totalRead[0]) {
				if (count != (message = exchangeReader.take())) {
					throw new RuntimeException(new ComparisonFailure("Out of order processing.", String.valueOf(count), String.valueOf(message)));
				}
			}
		});

		final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
		IntStream.range(0, load).forEach(publisher::put);

		readerTask.get();
		Assert.assertEquals(load, totalRead[0]);
	}

	//	@Test
	public void testTakeMinMax() throws Exception {

		final int min = 10;
		final int max = 10_000;
		final int[] totalRead = new int[1];

		final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
		ForkJoinTask<?> readerTask = ForkJoinPool.commonPool().submit(() -> {
			List<Integer> messages;
			for (int count = 0; count < load; Thread.yield(), count++, totalRead[0] += messages.size()) {
				messages = exchangeReader.take(min, max);
				if (messages.size() < min || messages.size() > max) {
					throw new RuntimeException("Out of polling range.");
				}
				for (int message : messages) {
					if (count++ != message) {
						throw new RuntimeException(new ComparisonFailure("Out of order processing.", String.valueOf(count), String.valueOf(message)));
					}
				}
			}
		});

		final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
		IntStream.range(0, load).forEach(publisher::put);

		readerTask.get();
		Assert.assertEquals(load, totalRead[0]);
	}

	//	@Test
	public void testTakeMin() throws Exception {

		final int min = 10;
		final int[] totalRead = new int[1];

		final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
		ForkJoinTask<?> readerTask = ForkJoinPool.commonPool().submit(() -> {
			List<Integer> messages;
			for (int count = 0; count < load; Thread.yield(), count++, totalRead[0] += messages.size()) {
				messages = exchangeReader.take(min);
				if (messages.size() < min) {
					throw new RuntimeException("Out of polling range.");
				}
				for (int message : messages) {
					if (count++ != message) {
						throw new RuntimeException(new ComparisonFailure("Out of order processing.", String.valueOf(count), String.valueOf(message)));
					}
				}
			}
		});

		final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
		IntStream.range(0, load).forEach(publisher::put);

		readerTask.get();
		Assert.assertEquals(load, totalRead[0]);
	}

	@Test
	public void testTakeConsumer() throws Exception {

		final Integer[] lastRead = new Integer[1];
		final int[] totalRead = new int[1];

		final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
		ForkJoinTask<?> readerTask = ForkJoinPool.commonPool().submit(() ->
			exchangeReader.take(i -> ((i == totalRead[0]++) && ((lastRead[0] = i) != (load - 1))))
		);

		final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
		IntStream.range(0, load).forEach(publisher::put);

		readerTask.get();
		Assert.assertEquals(Integer.valueOf(load - 1), lastRead[0]);
		Assert.assertEquals(load, totalRead[0]);
	}

	@Test
	public void testTakeConsumers() throws Exception {

		final Integer[] lastRead1 = new Integer[1];
		final int[] totalRead1 = new int[1];

		final Integer[] lastRead2 = new Integer[1];
		final int[] totalRead2 = new int[1];

		final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
		ForkJoinTask<?> readerTask = ForkJoinPool.commonPool().submit(() ->
			exchangeReader.take(
				i -> ((i == totalRead1[0]++) && ((lastRead1[0] = i) != (load - 1))),
				i -> ((i == totalRead2[0]++) && ((lastRead2[0] = i) != (load / 2)))
			)
		);

		final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
		IntStream.range(0, load).forEach(publisher::put);

		readerTask.get();
		Assert.assertEquals(Integer.valueOf(load - 1), lastRead1[0]);
		Assert.assertEquals(load, totalRead1[0]);
		Assert.assertEquals(Integer.valueOf((load / 2)), lastRead2[0]);
		Assert.assertEquals((load / 2) + 1, totalRead2[0]);

	}

	//	@Test
	public void testTakeMinMaxConsumer() throws Exception {

		final int min = 10_000;
		final int max = 100_000;

		final Integer[] lastRead1 = new Integer[1];
		final int[] totalRead1 = new int[1];

		final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
		ForkJoinTask<?> readerTask = ForkJoinPool.commonPool().submit(() ->
			exchangeReader.take(min, max, b -> {
				for (Integer i : b) {
					lastRead1[0] = i;
					if (totalRead1[0]++ != i) {
						return false;
					}
				}
				return b.size() >= min && b.size() <= max && lastRead1[0] != (load - 1);
			})
		);

		final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
		IntStream.range(0, load).forEach(publisher::put);

		readerTask.get();
		Assert.assertEquals(Integer.valueOf(load - 1), lastRead1[0]);
		Assert.assertEquals(load, totalRead1[0]);
	}

	//	@Test
	public void testTakeMinMaxConsumers() throws Exception {

		final int min = 10_000;
		final int max = 100_000;

		final Integer[] lastRead1 = new Integer[1];
		final int[] totalRead1 = new int[1];

		final Integer[] lastRead2 = new Integer[1];
		final int[] totalRead2 = new int[1];

		final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
		ForkJoinTask<?> readerTask = ForkJoinPool.commonPool().submit(() ->
			exchangeReader.take(min, max, b -> {
				for (Integer i : b) {
					lastRead1[0] = i;
					if (totalRead1[0]++ != i) {
						return false;
					}
				}
				return b.size() >= min && b.size() <= max && lastRead1[0] != (load - 1);
			}, b -> {
				for (Integer i : b) {
					lastRead2[0] = i;
					if (totalRead2[0]++ != i) {
						return false;
					}
				}
				return b.size() >= min && b.size() <= max && lastRead2[0] != (load / 2);
			})
		);

		final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
		IntStream.range(0, load).forEach(publisher::put);

		readerTask.get();
		Assert.assertEquals(Integer.valueOf(load - 1), lastRead1[0]);
		Assert.assertEquals(load, totalRead1[0]);
		Assert.assertEquals(Integer.valueOf(load / 2), lastRead2[0]);
		Assert.assertEquals(load / 2, totalRead2[0]);

	}

	//	@Test
	public void testTakeMinConsumer() throws Exception {

		final int min = 10_000;

		final Integer[] lastRead1 = new Integer[1];
		final int[] totalRead1 = new int[1];

		final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
		ForkJoinTask<?> readerTask = ForkJoinPool.commonPool().submit(() ->
			exchangeReader.take(min, b -> {
				for (Integer i : b) {
					lastRead1[0] = i;
					if (totalRead1[0]++ != i) {
						return false;
					}
				}
				return b.size() >= min && lastRead1[0] != (load - 1);
			})
		);

		final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
		IntStream.range(0, load).forEach(publisher::put);

		readerTask.get();
		Assert.assertEquals(Integer.valueOf(load - 1), lastRead1[0]);
		Assert.assertEquals(load, totalRead1[0]);
	}

	//	@Test
	public void testTakeMinConsumers() throws Exception {

		final int min = 10_000;

		final Integer[] lastRead1 = new Integer[1];
		final int[] totalRead1 = new int[1];

		final Integer[] lastRead2 = new Integer[1];
		final int[] totalRead2 = new int[1];

		final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
		ForkJoinTask<?> readerTask = ForkJoinPool.commonPool().submit(() ->
			exchangeReader.take(min, b -> {
				for (Integer i : b) {
					lastRead1[0] = i;
					if (totalRead1[0]++ != i) {
						return false;
					}
				}
				return b.size() >= min && lastRead1[0] != (load - 1);
			}, b -> {
				for (Integer i : b) {
					lastRead2[0] = i;
					if (totalRead2[0]++ != i) {
						return false;
					}
				}
				return b.size() >= min && lastRead2[0] != (load / 2);
			})
		);

		final MessageExchange.Writer<Integer> publisher = exchange.newWriter();
		IntStream.range(0, load).forEach(publisher::put);

		readerTask.get();
		Assert.assertEquals(Integer.valueOf(load - 1), lastRead1[0]);
		Assert.assertEquals(load, totalRead1[0]);
		Assert.assertEquals(Integer.valueOf(load / 2), lastRead2[0]);
		Assert.assertEquals(load / 2, totalRead2[0]);

	}

	//	@Test
	public void testTakeMultiProducersPerformance() throws Exception {
		if (isMultiProducer) {
			final MessageExchange.Reader<Integer> exchangeReader = exchange.getReader();
			final MessageExchange.Writer<Integer> exchangeWriter = exchange.newWriter();

			final AtomicInteger counter = new AtomicInteger();
			ForkJoinTask<?> reader = ForkJoinPool.commonPool().submit(() ->
				exchangeReader.take(i -> {
					counter.incrementAndGet();
					return counter.get() < load;
				})
			);

			ForkJoinTask<?> writer1 = ForkJoinPool.commonPool().submit(() ->
				IntStream.range(0, load / 2).map(i -> {
					Thread.yield();
					return i;
				}).forEach(exchangeWriter::put));

			ForkJoinTask<?> writer2 = ForkJoinPool.commonPool().submit(() ->
				IntStream.range(load / 2, load).map(i -> {
					Thread.yield();
					return i;
				}).forEach(exchangeWriter::put));

			writer1.get();
			writer2.get();
			reader.get();
			Assert.assertEquals(load, counter.get());
		}
	}
}