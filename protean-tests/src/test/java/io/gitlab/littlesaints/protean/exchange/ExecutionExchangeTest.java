/*
 *                     protean-tests
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-tests.
 *
 * protean-tests is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-tests is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.exchange;

import io.gitlab.littlesaints.protean.Constants;
import io.gitlab.littlesaints.protean.Constants.Exchange;
import io.gitlab.littlesaints.protean.Processor;
import io.gitlab.littlesaints.protean.configuration.Config;
import io.gitlab.littlesaints.protean.configuration.Props;
import io.gitlab.littlesaints.protean.event.State;
import io.gitlab.littlesaints.protean.exchanges.MessageExchange;
import io.gitlab.littlesaints.protean.exchanges.blocking.LinkedBlockingExchange;
import io.gitlab.littlesaints.protean.exchanges.blocking.LinkedSyncTransferExchange;
import io.gitlab.littlesaints.protean.exchanges.blocking.LinkedTransferExchange;
import io.gitlab.littlesaints.protean.exchanges.executor.OrderedExecutionExchange;
import io.gitlab.littlesaints.protean.exchanges.executor.UnOrderedExecutionExchange;
import io.gitlab.littlesaints.protean.processing.ProcessingTestsSupport;
import io.gitlab.littlesaints.protean.resources.Registrar;
import lombok.extern.log4j.Log4j2;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.ServiceLoader;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntFunction;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;
import java.util.stream.IntStream;

@RunWith(Parameterized.class)
@Log4j2
public class ExecutionExchangeTest {

    private final int load;
    private final MessageExchange<Integer> executor;
    private final boolean isMultiProducer;

    static {
        ServiceLoader.load(Registrar.class).forEach(r -> log.info("Loading {}", r));
    }

    public ExecutionExchangeTest(String executorExchange, String exchangeType, int concurrency, int capacity, int load, boolean isMultiProducer) {
        this.load = load;
        this.isMultiProducer = isMultiProducer;
        IntFunction<MessageExchange<Integer>> exchangeSupplier = c -> MessageExchange.REGISTRY.lookup(exchangeType).orElseThrow(() -> new RuntimeException("exchangeType lookup failed !!"))
                .newInstance(c, isMultiProducer, new Config());

        final Props props = new Props();
        props.put("name", "test::" + executorExchange + "::test-worker");
        //TODO: implement lazy routes, then turn this on
        props.put(Exchange.LAZY_ROUTES, false);
        Supplier<Processor<Integer>> fxSupplier = () -> Processor.of(i -> State.SUCCESS, () -> {
        });
        props.put(Constants.CONCURRENCY, concurrency);
        props.put(Exchange.EXCHANGE_SUPPLIER, exchangeSupplier);
        props.put(Exchange.PROCESSOR_SUPPLIER, fxSupplier);
        switch (executorExchange) {
            case OrderedExecutionExchange.TYPE:
                ToIntFunction<Integer> routeResolver = i -> i % concurrency;
                props.put(Exchange.ORDER_RESOLVER, routeResolver);
                break;
            default:
                break;
        }
        executor = MessageExchange.REGISTRY.lookup(executorExchange).orElseThrow(() -> new RuntimeException("execution exchange lookup failed !!"))
                .newInstance(capacity, isMultiProducer, new Config(props));
    }

    // name attribute is optional, provide an unique name for test
    // multiple parameters, uses Collection<Object[]>
    @Parameterized.Parameters(name = "{index}: executorExchange={0} | exchange={1} | concurrency={2} | capacity={3} | load={4} | isMultiProducer={5}")
    public static Collection<Object[]> data() {

        final int lf = ProcessingTestsSupport.LOAD_FACTOR_RESOLVER.getAsInt();
        return Arrays.asList(new Object[][]{

                {OrderedExecutionExchange.TYPE, LinkedBlockingExchange.TYPE, 4, 8 * lf, 250 * lf, true},
                {OrderedExecutionExchange.TYPE, LinkedBlockingExchange.TYPE, 4, 8 * lf, 250 * lf, false},
                {UnOrderedExecutionExchange.TYPE, LinkedBlockingExchange.TYPE, 4, 8 * lf, 250 * lf, true},
                {UnOrderedExecutionExchange.TYPE, LinkedBlockingExchange.TYPE, 4, 8 * lf, 250 * lf, false},

                {OrderedExecutionExchange.TYPE, LinkedTransferExchange.TYPE, 4, 8 * lf, 250 * lf, true},
                {OrderedExecutionExchange.TYPE, LinkedTransferExchange.TYPE, 4, 8 * lf, 250 * lf, false},
                {UnOrderedExecutionExchange.TYPE, LinkedTransferExchange.TYPE, 4, 8 * lf, 250 * lf, true},
                {UnOrderedExecutionExchange.TYPE, LinkedTransferExchange.TYPE, 4, 8 * lf, 250 * lf, false},

                {OrderedExecutionExchange.TYPE, LinkedSyncTransferExchange.TYPE, 4, 8 * lf, 250 * lf, true},
                {OrderedExecutionExchange.TYPE, LinkedSyncTransferExchange.TYPE, 4, 8 * lf, 250 * lf, false},
                {UnOrderedExecutionExchange.TYPE, LinkedSyncTransferExchange.TYPE, 4, 8 * lf, 250 * lf, true},
                {UnOrderedExecutionExchange.TYPE, LinkedSyncTransferExchange.TYPE, 4, 8 * lf, 250 * lf, false}

        });
    }

    //	@Test(timeout = 20_000L)
    @Test
    public void testAccuracy() throws Exception {

        final MessageExchange.Reader<Integer> exchangeReader = executor.getReader();
        final MessageExchange.Writer<Integer> exchangeWriter = executor.newWriter();

        final AtomicInteger counter = new AtomicInteger();
        ForkJoinTask<?> reader = ForkJoinPool.commonPool().submit(() -> {
            for (int count = 0; count < load; Thread.yield(), count++) {
                exchangeReader.take();
                counter.incrementAndGet();
            }
//			exchangeReader.take(i -> counter.incrementAndGet() != load);
        });

        Thread.sleep(1000);
        IntStream.range(0, load).forEach(exchangeWriter::put);

        reader.get();
        Assert.assertEquals(load, counter.get());
    }

    //	@Test(timeout = 20_000L)
    @Test
    public void testMultiAccuracy() throws Exception {
        if (isMultiProducer) {
            final MessageExchange.Reader<Integer> exchangeReader = executor.getReader();
            final MessageExchange.Writer<Integer> exchangeWriter = executor.newWriter();

            final AtomicInteger counter = new AtomicInteger();
            ForkJoinTask<?> reader = ForkJoinPool.commonPool().submit(() -> {
                for (int count = 0; count < load; Thread.yield(), count++) {
                    exchangeReader.take();
                    counter.incrementAndGet();
                }
            });

            ForkJoinTask<?> writer1 = ForkJoinPool.commonPool().submit(() ->
                    IntStream.range(0, load / 2).peek(i -> Thread.yield()).forEach(exchangeWriter::put));

            ForkJoinTask<?> writer2 = ForkJoinPool.commonPool().submit(() ->
                    IntStream.range(load / 2, load).peek(i -> Thread.yield()).forEach(exchangeWriter::put));

            writer1.get();
            writer2.get();
            reader.get();
            Assert.assertEquals(load, counter.get());
        }
    }

}
