/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.exchanges.executor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;

import io.gitlab.littlesaints.protean.Processor;
import io.gitlab.littlesaints.protean.exchanges.AbstractMessageExchange;
import io.gitlab.littlesaints.protean.exchanges.MessageExchange;

/**
 * @param <E> The getType of objects to execute upon.
 * @author Varun Anand
 * @since 1.0
 */
abstract class AbstractExecutionExchange<E> extends AbstractMessageExchange<E> {

	private final String name;
	final int concurrency;
	final Supplier<Processor<E>> processorSupplier;
	final IntFunction<MessageExchange<E>> exchangeSupplier;

	AbstractExecutionExchange(String name, Supplier<Processor<E>> processorSupplier, int concurrency, IntFunction<MessageExchange<E>> exchangeSupplier) {
		this.name = name;
		this.processorSupplier = processorSupplier;
		this.concurrency = concurrency;
		this.exchangeSupplier = exchangeSupplier;
	}

	protected abstract class _ExecutionExchangeReader implements MessageExchange.Reader<E> {

		abstract Reader<E> getOutputExchangeReader(int idx);

		@Override
		public E poll() {
			E result = null;
			for (int i = 0; i < concurrency; i++) {
				if (getOutputExchangeReader(i) != null) {
					result = getOutputExchangeReader(i).poll();
					if (result != null) {
						break;
					}
				}
			}
			return result;
		}

		@Override
		public List<E> poll(int min, int max, int maxAttempts) {
			List<E> batch = null;
			int currentBatchSize = 0;
			List<E> result;
			final boolean[] doContinue = new boolean[]{true};
			for (int attempts = 0; doContinue[0] && attempts < maxAttempts; attempts++, Thread.yield()) {
				for (int i = 0; doContinue[0] && i < concurrency; i++) {
					if (getOutputExchangeReader(i) != null) {
						result = getOutputExchangeReader(i).poll(max - currentBatchSize);
						if (!result.isEmpty()) {
							if (batch == null) {
								batch = new ArrayList<>(min);
							}
							batch.addAll(result);
							currentBatchSize = batch.size();
							if (batch.size() == max) {
								doContinue[0] = false;
							}
							attempts = 0;
						} else if (batch != null && currentBatchSize >= min) {
							doContinue[0] = false;
						}
					}
				}
			}
			return batch == null ? Collections.emptyList() : batch;
		}

		@Override
		public List<E> poll(int max) {
			List<E> batch = null;
			int currentBatchSize = 0;
			List<E> result;
			final boolean[] doContinue = new boolean[]{true};
			for (int i = 0; doContinue[0] && i < concurrency; i++) {
				if (getOutputExchangeReader(i) != null) {
					result = getOutputExchangeReader(i).poll(max - currentBatchSize);
					if (!result.isEmpty()) {
						if (batch == null) {
							batch = new ArrayList<>();
						}
						batch.addAll(result);
						currentBatchSize = batch.size();
						if (currentBatchSize == max) {
							doContinue[0] = false;
						}
					}
				}
			}
			return batch == null ? Collections.emptyList() : batch;
		}

		@Override
		public boolean poll(int maxAttempts, Predicate<? super E> consumer) {
			int attempts = 0;
			for (; attempts < maxAttempts; attempts++) {
				for (int idx = 0; idx < concurrency; idx++) {
					if (getOutputExchangeReader(idx) != null) {
						if (getOutputExchangeReader(idx).poll(1, consumer)) {
							return true;
						}
					}
				}
			}
			return false;
		}

		@Override
		public E take() {
			E result;
			while ((result = poll()) == null) {
				//TODO: wait strategy ??
				Thread.yield();
			}
			return result;
		}

		@Override
		public List<E> take(int min, int max) {
			List<E> batch = null;
			int currentBatchSize = 0;
			List<E> result;
			final boolean[] doContinue = new boolean[]{true};
			for (; doContinue[0]; Thread.yield()) {
				for (int i = 0; doContinue[0] && i < concurrency; i++) {
					if (getOutputExchangeReader(i) != null) {
						result = getOutputExchangeReader(i).poll(max - currentBatchSize);
						if (!result.isEmpty()) {
							if (batch == null) {
								batch = new ArrayList<>(min);
							}
							batch.addAll(result);
							currentBatchSize = batch.size();
							if (batch.size() == max) {
								doContinue[0] = false;
							}
						} else if (batch != null && currentBatchSize >= min) {
							doContinue[0] = false;
						}
					}
				}
			}
			return batch == null ? Collections.emptyList() : batch;
		}

		@Override
		public List<E> take(int min) {
			List<E> batch = null;
			List<E> result;
			final boolean[] doContinue = new boolean[]{true};
			for (; doContinue[0]; Thread.yield()) {
				for (int i = 0; doContinue[0] && i < concurrency; i++) {
					if (getOutputExchangeReader(i) != null) {
						result = getOutputExchangeReader(i).poll(Integer.MAX_VALUE);
						if (!result.isEmpty()) {
							if (batch == null) {
								batch = new ArrayList<>(min);
							}
							batch.addAll(result);
						} else if (batch != null && batch.size() >= min) {
							doContinue[0] = false;
						}
					}
				}
			}
			return batch == null ? Collections.emptyList() : batch;
		}

		@Override
		public void take(Predicate<? super E> consumer) {
			for (; ; Thread.yield()) {
				for (int i = 0; i < concurrency; ) {
					if (getOutputExchangeReader(i) != null) {
						//TODO: remove hard-coding
						if (getOutputExchangeReader(i).poll(2, consumer)) {
							return;
						}
					}
				}
			}
		}

	}

	public String getName() {
		return name;
	}

}
