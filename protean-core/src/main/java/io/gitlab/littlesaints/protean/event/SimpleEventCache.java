/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.event;

import io.gitlab.littlesaints.protean.EventExecutor;
import io.gitlab.littlesaints.protean.configuration.Config;
import io.gitlab.littlesaints.protean.configuration.Props;
import io.gitlab.littlesaints.protean.resources.Registrar;
import io.gitlab.littlesaints.protean.throttle.Throttle;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.function.Function;
import java.util.stream.IntStream;

/**
 * A minimal Events cache used by the {@link EventExecutor}, if it's configured for caching. This acts as a implicit {@link Throttle} as well.
 */
public class SimpleEventCache<E> implements EventCache<E> {

	public static final String TYPE = "protean::event::cache::simple";

	public static class _Registrar implements Registrar {
		static {
			REGISTRY.register(TYPE, SimpleEventCache::new);
		}

		@Override
		public String toString() {
			return "EventCache::Registrar";
		}
	}

	private static final String POOL_INDEX_PROP = "contextPoolIndex";

	private final AtomicReferenceArray<Boolean> usedEventIndexes;

	private final E[] events;

	private final int capacity;

	private AtomicInteger nextFetchIndex;

	private final EventAccessor<E, ?> eventAccessor;

	private final Semaphore permits;

	public SimpleEventCache(Config config, int capacity, Function<Props, E> constructor, EventAccessor<E, ?> eventAccessor) {
        this.capacity = capacity;
	    permits = new Semaphore(capacity);
		events = (E[]) new Object[capacity];
		usedEventIndexes = new AtomicReferenceArray<>(capacity);
		IntStream.range(0, capacity).forEach(i -> {
			Props props = new Props();
			props.put(POOL_INDEX_PROP, i);
			events[i] = constructor.apply(props);
			usedEventIndexes.set(i, false);
		});
		nextFetchIndex = new AtomicInteger();
		this.eventAccessor = eventAccessor;
	}

	@Override
	public E get() {
		while (true) {
            try {
                permits.acquire();
                break;
            } catch (InterruptedException e) {
                //TODO: log error
            }
        }
        E event;
        while ((event = tryGet()) == null);
		return event;
	}

	@Override
	public E tryGet() {
		int idx = nextFetchIndex.get();
		for (int counter = 0; counter < capacity; counter++, idx++) {
			if (!usedEventIndexes.get(idx)) {
				if (usedEventIndexes.compareAndSet(idx, false, true)) {
					nextFetchIndex.lazySet(idx + 1 == capacity ? 0 : idx + 1);
					return events[idx];
				}
			} else if (idx == capacity - 1) {
				idx = -1;
			}
		}
		return null;
	}

	@Override
	public void put(E event) {
		usedEventIndexes.set(eventAccessor.getConf(event, POOL_INDEX_PROP), false);
		permits.release();
	}

}
