/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.executors.sync;

import io.gitlab.littlesaints.protean.Graph;
import io.gitlab.littlesaints.protean.Node;
import io.gitlab.littlesaints.protean.Processor;
import io.gitlab.littlesaints.protean.analytics.StatsListener;
import io.gitlab.littlesaints.protean.event.EventAccessor;
import io.gitlab.littlesaints.protean.event.State;
import lombok.extern.log4j.Log4j2;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static io.gitlab.littlesaints.protean.EventExecutor.*;

/**
 * The class builds a {@link Stream} to continually process the incoming events.
 * It's used by the {@link SyncExecutor} to prepare the processing workflow / pipeline as per the {@link Graph} configuration.
 *
 * @author Varun Anand
 * @since 1.0
 */
@Log4j2
class SyncExecutorConfigurer<E> {

    private final String contextName;

    private final Graph<E> graph;

    private final Set<String> configuredStages;

    private final Map<String, Node<E>> nodes;

    private final Map<String, Collection<String>> dependencies;

    private final Map<String, Collection<String>> hierarchy;

    private final Queue<String> queue;

    private final StatsListener<E> statsListener;

    private final EventAccessor<E, ?> eventAccessor;

    SyncExecutorConfigurer(String contextName, Graph<E> graph, StatsListener<E> statsListener, EventAccessor<E, ?> eventAccessor) {
        this.contextName = contextName;
        this.graph = graph;
        nodes = graph.getNodesMap();
        dependencies = graph.getDependencies();
        hierarchy = graph.getHierarchy();
        queue = new ArrayDeque<>(dependencies.size());
        configuredStages = new HashSet<>();
        this.statsListener = statsListener;
        this.eventAccessor = eventAccessor;
    }

    Function<E, E> configure(Collection<Runnable> closeActions) {
        configuredStages.clear();
        queue.clear();
        queue.addAll(graph.getRoots());

        String stageName;
        Function<E, E> function = e -> e;
        while (!queue.isEmpty()) {
            stageName = queue.remove();
            if (!configuredStages.contains(stageName)) {
                if (!dependencies.containsKey(stageName) || configuredStages.containsAll(dependencies.get(stageName))) {
                    final Node<E> node = nodes.get(stageName);
                    if (node.getConcurrency() > 1) {
                        log.warn("Executor " + contextName + " :: the configured executor doesn't support parallelism and this configuration will be ignored. " +
                                "It's a synchronous executor, where the publisher thread executes the workflow serially. ");
                    }
                    function = function.andThen(configureSerialStageProcessing(node, closeActions));
                    configuredStages.add(stageName);
                    if (hierarchy.containsKey(stageName)) {
                        queue.addAll(hierarchy.get(stageName));
                    }
                } else {
                    queue.add(stageName);
                }
            }
        }

        return function;
    }

    private Function<E, E> configureSerialStageProcessing(Node<E> node, Collection<Runnable> closeActions) {
        final boolean skippable = node.isSkippable();
        //TODO: can we do better than thread local ?
        final ThreadLocal<Function<E, State>> processorRef = ThreadLocal.withInitial(() -> {
            Processor<E> p = node.getProcessorSupplier().get();
            closeActions.add(p::close);
            return newExecutionOp(p.getFunction(), node.getExceptionHandler(), node.getRetryStrategy());
        });

        final Function<E, E> function;
        if (node.getFilter() == null) {
            function = e -> {
                if (filter(e, skippable, null, eventAccessor::getState)) {
                    process(processorRef.get(), e, eventAccessor::getState, eventAccessor::setState);
                }
                return e;
            };
        } else {
            final ThreadLocal<Predicate<E>> filterRef = ThreadLocal.withInitial(() -> node.getFilter().get());
            function = e -> {
                if (filter(e, skippable, filterRef.get(), eventAccessor::getState)) {
                    process(processorRef.get(), e, eventAccessor::getState, eventAccessor::setState);
                }
                return e;
            };
        }

        if (statsListener == null) {
            return function;
        } else {
            return e -> {
                long startMillis = System.currentTimeMillis();
                return function.andThen(ev -> {
                    statsListener.eventProcessed(contextName, e, node.getName(), startMillis, System.currentTimeMillis());
                    return ev;
                }).apply(e);
            };
        }
    }

}
