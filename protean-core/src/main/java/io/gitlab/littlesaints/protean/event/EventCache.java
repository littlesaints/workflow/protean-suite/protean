/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.event;

import io.gitlab.littlesaints.protean.configuration.Config;
import io.gitlab.littlesaints.protean.configuration.Props;
import io.gitlab.littlesaints.protean.resources.Registry;

import java.util.function.Function;

/**
 * @author Varun Anand
 * @since 1.0
 */
public interface EventCache<E> {

	/**
	 * Repository for all configured executors.
	 */
	Registry<Factory> REGISTRY = new Registry<>();

	E get();

	E tryGet();

	void put(E event);

	/**
	 * The factory method for all implementations.
	 * This method will be configured in {@link #REGISTRY} to facilitate lookup and creation of instance by their {@link String} getType.
	 */
	interface Factory {
		<E> EventCache<E> newInstance(Config config, int capacity, Function<Props, E> constructor, EventAccessor<E, ?> eventAccessor);
	}
}
