/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.executors.sync;

import io.gitlab.littlesaints.protean.EventExecutor;
import io.gitlab.littlesaints.protean.Graph;
import io.gitlab.littlesaints.protean.analytics.StatsListener;
import io.gitlab.littlesaints.protean.configuration.ExecutorOptions;
import io.gitlab.littlesaints.protean.event.EventAccessor;
import io.gitlab.littlesaints.protean.resources.Registrar;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * This is a Synchronous {@link EventExecutor}.
 * It uses the publisher thread to execute the configured graph stages i.e. the workflow, serially. There can be multiple publisher threads in the application.
 * It ignores setting for parallelism and concurrency at executor and node levels. There are other available executors that better fit the parallel execution requirements.
 *
 * @author Varun Anand
 * @since 1.0
 */
@Log4j2
public class SyncExecutor<E, P, R> implements EventExecutor<P, R> {

    public static final String TYPE = "protean::executor::sync";

    public static class _Registrar implements Registrar {
        static {
            REGISTRY.register(TYPE, SyncExecutor::new);
        }

        @Override
        public String toString() {
            return "SyncExecutor::Registrar";
        }
    }

    private final Function<P, R> executeAction;
    private final Runnable shutdownAction;

    private SyncExecutor(final String contextName, final ExecutorOptions executorOptions, Supplier<E> constructor, final Graph<E> graph,
                         final BiConsumer<E, P> initializer, final Consumer<E> ackListener, final EventAccessor<E, P> eventAccessor,
                         final StatsListener<E> statsListener, final boolean cleanShutdown, final Function<E, R> resultMapper) {

        final SyncExecutorConfigurer<E> eConfigurer = new SyncExecutorConfigurer<>(contextName, graph, statsListener, eventAccessor);
        final Collection<Runnable> closeActions = Collections.synchronizedList(new ArrayList<>());
        final Function<E, E> function = eConfigurer.configure(closeActions);

        final Function<E, E> syncAckListener;
        if (cleanShutdown) {
            AtomicBoolean continueProcessing = new AtomicBoolean(true);
            LongAdder inProcessingCounter = new LongAdder();
            final Consumer<E> ackFx = ackListener.andThen(e -> inProcessingCounter.decrement());
            syncAckListener = event -> {
                ackFx.accept(event);
                return event;
            };
            final Function<E, R> execFx = function.andThen(syncAckListener).andThen(resultMapper);
            executeAction = p -> {
                if (continueProcessing.get()) {
                    inProcessingCounter.increment();
                    if (continueProcessing.get()) {
                        E event = constructor.get();
                        initializer.accept(event, p);
                        return execFx.apply(event);
                    } else {
                        inProcessingCounter.decrement();
                    }
                } else {
                    while (inProcessingCounter.sum() == 0) {
                        Thread.yield();
                    }
                }
                return null;
            };
            shutdownAction = () -> {
                continueProcessing.lazySet(false);
                //todo: get rid of processingCounter
//				continueProcessing.set(false);
                while (inProcessingCounter.sum() != 0) {
                    Thread.yield();
                }
                closeActions.forEach(Runnable::run);
            };

        } else {
            syncAckListener = e -> {
                ackListener.accept(e);
                return e;
            };
            final Function<E, R> execFx = function.andThen(syncAckListener).andThen(resultMapper);
            executeAction = p -> {
                E event = constructor.get();
                initializer.accept(event, p);
                return execFx.apply(event);
            };
            shutdownAction = () -> closeActions.forEach(Runnable::run);
        }
    }

    @Override
    public R execute(final P payload) {
        return executeAction.apply(payload);
    }

    @Override
    public void shutdown() {
        shutdownAction.run();
    }

}
