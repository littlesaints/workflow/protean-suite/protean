/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.gitlab.littlesaints.protean.EventExecutor;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Options to common an {@link EventExecutor}
 *
 * @author Varun Anand
 * @see EventExecutor
 * @since 0.1
 */
@Getter
@Setter
@ToString
public class ExecutorOptions {

	private GenericOptions type = GenericOptions.builder().supplier(Defaults.EXECUTOR).build();

	private int buffer = Defaults.BUFFER;

	@JsonProperty("multi-producer")
	private boolean multiProducer;

	private GenericOptions exchange = GenericOptions.builder().supplier(Defaults.EXCHANGE).build();

	private ExecutorModeOptions mode = new ExecutorModeOptions();
}