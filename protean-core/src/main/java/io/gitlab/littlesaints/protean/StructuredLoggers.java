/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean;

import java.util.function.Supplier;

public final class StructuredLoggers {

    public static Supplier<Format> JSON = JsonFormat::new;

    private static final class JsonFormat implements Format {

        private static final String QUOTE = "\"";
        private static final String COLON = ":";
        private static final String DELIMITER = ", ";
        private static final String PLACE_HOLDER = "{}";

        private final StringBuilder builder = new StringBuilder("{");
        private boolean isFirst = true;

        public <T> JsonFormat bind(String key, Class<T> clazz) {
            if (isFirst) {
                isFirst = false;
            } else {
                builder.append(DELIMITER);
            }
            builder.append(QUOTE).append(key).append(QUOTE).append(COLON);
            if (clazz.equals(String.class)) {
                builder.append(QUOTE).append(PLACE_HOLDER).append(QUOTE);
            } else {
                builder.append(PLACE_HOLDER);
            }
            return this;
        }

        public String build() {
            return builder.append("}").toString();
        }

    }

    public interface Format {
        <T> Format bind(String key, Class<T> clazz);
        String build();
    }

}
