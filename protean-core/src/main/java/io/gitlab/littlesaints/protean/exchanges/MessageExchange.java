/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.exchanges;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import io.gitlab.littlesaints.protean.configuration.Config;
import io.gitlab.littlesaints.protean.resources.Registry;

/**
 * This is a queue for exchanging messages between producers and consumers.
 * <br>
 * The implementations are wrapper over existing queue implementations. They hide away the knowledge/code required to efficiently use them.
 * The applications can use an implementation or switch to another, without make any code changes.
 * It can also choose implementations based on their use case and whether the underlying has a different strategy for multiple or single producers.
 * <br>
 * It supports a variety of blocking and non-blocking operations.
 * <br>
 * All implementations support the notion of a terminal state. When a terminal element is submitted to the exchange, all the readers will receive it, marking the end of all future transmission.
 *
 * @param <E> The getType of messages in the exchange.
 * @author Varun Anand
 * @since 1.0
 */
public interface MessageExchange<E> {

	/**
	 * Repository for all configured executors.
	 */
	Registry<Factory> REGISTRY = new Registry<>();

	/**
	 * The factory method for all implementations.
	 * This method will be configured in {@link #REGISTRY} to facilitate lookup and creation of instance by their {@link String} getType.
	 */
	interface Factory {
		<E> MessageExchange<E> newInstance(final int capacity, final boolean isMultiProducer, final Config props);
	}

	/**
	 * Provides an instance of {@link Reader} for this exchange. The implementation can return a cached instance, if it's thread-safe.
	 *
	 * @return a reader
	 */
	Reader<E> getReader();

	/**
	 * Provides an instance of {@link Writer} for this exchange. The implementation can return a cached instance, if it's thread-safe.
	 *
	 * @return a writer
	 */
	Writer<E> newWriter();

	boolean isEmpty();

	void shutdown();

	/**
	 * An interface to read from a resource.
	 * <br>
	 * Access by different threads  - The implementations don't support concurrent access but can be accessed serially.
	 * However, there are methods to support pub-sub <i>like</i> scenarios.
	 *
	 * @param <E> the getType of event expected to be read.
	 */
	interface Reader<E> {

		/**
		 * Try to retrieve an element. This is a non-blocking operation.
		 *
		 * @return a message if available, or null otherwise.
		 */
		E poll();

		/**
		 * Try to retrieve an element. This is a non-blocking operation.
		 *
		 * @param min         the minimum elements to read, unless maxAttempts have exhausted.
		 * @param max         the maximum elements to read
		 * @param maxAttempts the maximum attempts to read before returning a {@link Collections#emptyList()}
		 * @return a {@link List} of elements, if they're available until shutdown or maxAttempts are exhausted, or {@link Collections#emptyList()} otherwise.
		 */
		List<E> poll(int min, int max, int maxAttempts);

		default List<E> poll(int max, int maxAttempts) {
			List<E> messages;
			for (int i = 0; i < maxAttempts; i++) {
				messages = poll(max);
				if (!messages.isEmpty()) {
					return messages;
				} else if (messages == Collections.emptyList()) {
					break;
				}
			}
			return Collections.emptyList();
		}

		/**
		 * Try to retrieve a number of element until available and the maximum limit is not reached. This is a non-blocking operation.
		 *
		 * @param max the maximum elements to read
		 * @return a {@link List} of elements, if they're available until maximum limit is reached, or {@link Collections#emptyList()} otherwise.
		 */
		List<E> poll(int max);

		boolean poll(int maxAttempts, Predicate<? super E> consumer);

		default boolean poll(int maxAttempts, Predicate<? super E>... consumers) {
			final List<Predicate<? super E>> activeConsumers = Arrays.stream(consumers).collect(Collectors.toList());
			return poll(maxAttempts, e -> !(activeConsumers.removeIf(c -> !c.test(e)) && activeConsumers.isEmpty()));
		}

		default boolean poll(int min, int max, int maxAttempts, Predicate<List<? super E>> consumer) {
			for (List<? super E> messages; ; ) {
				messages = poll(min, max, maxAttempts);
				if (messages.isEmpty()) {
					return false;
				} else if (!consumer.test(messages)) {
					return true;
				}
			}
		}

		default boolean poll(int min, int max, int maxAttempts, Predicate<List<? super E>>... consumers) {
			final List<Predicate<List<? super E>>> activeConsumers = Arrays.stream(consumers).collect(Collectors.toList());
			return poll(min, max, maxAttempts, e -> !(activeConsumers.removeIf(c -> !c.test(e)) && activeConsumers.isEmpty()));
//			final Map<Predicate<List<? super E>>, Boolean> consumerMap = new HashMap<>();
//			Arrays.stream(consumers).forEach(c  -> consumerMap.put(c, true));
//			return poll(min, max, maxAttempts,
//					e -> consumerMap.entrySet().stream().filter(Map.Entry::getValue).map(Map.Entry::getKey).anyMatch(c -> {
//						if (c.test(e)) {
//							return true;
//						} else {
//							consumerMap.put(c, false);
//							return false;
//						}
//					}));
		}

		/**
		 * Try to retrieve a number of element until available and the maximum limit is not reached. This is a non-blocking operation.
		 *
		 * @param max
		 * @param maxAttempts
		 * @param consumer
		 * @return 'false' when there are no messages to read and the max attempts are exhausted
		 * or 'true' when consumer doesn't wish to consume further i.e. the consumer predicate returns false.
		 */
		default boolean poll(int max, int maxAttempts, Predicate<List<? super E>> consumer) {
			for (List<? super E> messages; ; ) {
				messages = poll(max, maxAttempts);
				if (messages.isEmpty()) {
					return false;
				} else if (!consumer.test(messages)) {
					return true;
				}
			}
		}

		default boolean poll(int max, int maxAttempts, Predicate<List<? super E>>... consumers) {
			final List<Predicate<List<? super E>>> activeConsumers = Arrays.stream(consumers).collect(Collectors.toList());
			return poll(max, maxAttempts, e -> !(activeConsumers.removeIf(c -> !c.test(e)) && activeConsumers.isEmpty()));
		}

		/**
		 * Retrieve a message or block the call otherwise.
		 *
		 * @return the element if one is readily available or blocking the call until one is available or null if the exchange has been shutdown.
		 * The application need not check for null unless it's a valid value or it intends to shutdown the exchange in some case.
		 */
		E take();

		/**
		 * Try to retrieve an element. This is a blocking operation.
		 *
		 * @param min the minimum elements to read
		 * @param max the maximum elements to read
		 * @return a {@link List} of elements, if they're available until a terminal state is read or maxAttempts are exhausted, or {@link Collections#emptyList()} otherwise.
		 */
//		default List<E> take(int min, int max) {
//			return poll(min, max, INFINITE);
//		}
		List<E> take(int min, int max);

		/**
		 * Try to retrieve an element. This is a blocking operation.
		 *
		 * @param min the minimum elements to read
		 * @return a {@link List} of elements, if they're available until a terminal state is read or maxAttempts are exhausted, or {@link Collections#emptyList()} otherwise.
		 */
//		default List<E> take(int min) {
//			return take(min, INFINITE);
//		}
		List<E> take(int min);

		/**
		 * Retrieve a message and forward it to the consumer, if one is available or block the call otherwise.
		 *
		 * @param consumer the consumer for the elements. It'll receive the elements until a terminal state is encountered or the consumer returns <i>false</i>.
		 */
		void take(Predicate<? super E> consumer);

		/**
		 * Retrieve a message and forward it to the consumer, if one is available or block the call otherwise.
		 *
		 * @param consumers the consumers for the elements. They'll receive the elements until a terminal state is encountered or the consumer returns <i>false</i>.
		 */
		default void take(Predicate<? super E>... consumers) {
			final List<Predicate<? super E>> activeConsumers = Arrays.stream(consumers).collect(Collectors.toList());
			take(e -> !(activeConsumers.removeIf(c -> !c.test(e)) && activeConsumers.isEmpty()));
		}

		default void take(int min, int max, Predicate<List<E>> consumer) {
			for (List<E> messages; ((messages = take(min, max)) != Collections.emptyList()) && consumer.test(messages); Thread.yield()) ;
		}

		/**
		 * Retrieve a message and forward it to the consumers, if one is available or block the call otherwise.
		 *
		 * @param min       the minimum elements to read
		 * @param max       the maximum elements to read
		 * @param consumers the consumers for the elements. They'll receive the elements until a terminal state is encountered or the consumer returns <i>false</i>.
		 */
		default void take(int min, int max, Predicate<List<E>>... consumers) {
			final List<Predicate<List<E>>> activeConsumers = Arrays.stream(consumers).collect(Collectors.toList());
			take(min, max, b -> !(activeConsumers.removeIf(c -> !c.test(b)) && activeConsumers.isEmpty()));
		}

		default void take(int min, Predicate<List<E>> consumer) {
			for (List<E> messages; ((messages = take(min)) != Collections.emptyList()) && consumer.test(messages); Thread.yield()) ;
		}

		default void take(int min, Predicate<List<E>>... consumers) {
			final List<Predicate<List<E>>> activeConsumers = Arrays.stream(consumers).collect(Collectors.toList());
			take(min, b -> !(activeConsumers.removeIf(c -> !c.test(b)) && activeConsumers.isEmpty()));
		}

	}

	interface Writer<E> {

		/**
		 * put an element in the exchange.
		 *
		 * @param e the element to add.
		 */
		boolean put(E e);

	}

}
