/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.littlesaints.protean.functions.streams.Try;
import io.gitlab.littlesaints.protean.Context;
import io.gitlab.littlesaints.protean.context.ContextBuilder;
import io.gitlab.littlesaints.protean.resources.Registrar;
import io.gitlab.littlesaints.protean.resources.ResourceLocator;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

import java.io.FileReader;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

/**
 * This represents the configuration for all configured {@link Context}s.
 * <p>
 * Use the methods newInstance prefix 'load', to create an instance from any supported format. This instance can be used to look up a configured {@link Context} using {@link #getContext(String)}.
 *
 * @see Context
 *
 * @author Varun Anand
 * @since 1.0
 */
@Log4j2
public final class Configuration {

    static {
        ServiceLoader.load(Registrar.class).forEach(r -> log.info("Loading {}", r));
    }

    public static final ResourceLocator LOCATOR = new ResourceLocator() {

        @Override
        public <T> Optional<T> doLookup(String name) {
            return REGISTRY.stream()
                    .map(l -> l.<T>lookup(name))
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .findAny();
        }
    };

	private static final ObjectMapper MAPPER = new ObjectMapper();
	private static final ObjectMapper YAML_MAPPER = new ObjectMapper(new YAMLFactory());

	//TODO: Configuration Registry to auto-load pre-built configurations and workflows. These can then be merged and connected newInstance app specific ones.

	@Getter
	@Setter
	private Map<String, ContextOptions> contexts;

	private final Map<String, ContextOptions> _contextOptions = new ConcurrentHashMap<>();
	private final Map<String, Context<?, ?>> _cachedContexts = new ConcurrentHashMap<>();

    /**
     * Loads the Configuration from json text.
     **/
    public static Configuration loadJsonText(String jsonText) throws Exception {
        final Configuration configuration = MAPPER.readValue(jsonText, Configuration.class);
        configuration._contextOptions.putAll(configuration.contexts);
        return configuration;
    }

    /**
     * Loads the Configuration from json file.
     **/
    public static Configuration loadJsonFile(String fileName) throws Exception {
        final Configuration configuration = MAPPER.readValue(new FileReader(fileName), Configuration.class);
        configuration._contextOptions.putAll(configuration.contexts);
        return configuration;
    }

    /**
     * Loads the Configuration from json file on classpath or further within a jar.
     **/
    public static Configuration loadJsonFileFromClasspath(Class aClass, String fileName) throws Exception {
        final Configuration configuration = MAPPER.readValue(aClass.getResourceAsStream(fileName.startsWith("/") ? fileName : "/".concat(fileName)), Configuration.class);
        configuration._contextOptions.putAll(configuration.contexts);
        return configuration;
    }

    /**
     * Loads the Configuration from yaml text.
     **/
    public static Configuration loadYamlText(String jsonText) throws Exception {
        final Configuration configuration = YAML_MAPPER.readValue(jsonText, Configuration.class);
        configuration._contextOptions.putAll(configuration.contexts);
        return configuration;
    }

    /**
     * Loads the Configuration from yaml file.
     **/
    public static Configuration loadYamlFile(String fileName) throws Exception {
        final Configuration configuration = YAML_MAPPER.readValue(new FileReader(fileName), Configuration.class);
        configuration._contextOptions.putAll(configuration.contexts);
        return configuration;
    }

    /**
     * Loads the Configuration from yaml file on classpath or further within a jar.
     **/
    public static Configuration loadYamlFileFromClasspath(Class aClass, String fileName) throws Exception {
        final Configuration configuration = YAML_MAPPER.readValue(aClass.getResourceAsStream(fileName.startsWith("/") ? fileName : "/".concat(fileName)), Configuration.class);
        configuration._contextOptions.putAll(configuration.contexts);
        return configuration;
    }

	public Configuration merge(Configuration configurations) {
        _contextOptions.putAll(configurations._contextOptions);
		_cachedContexts.putAll(configurations._cachedContexts);
		return this;
	}

	public static Configuration merge(Configuration... configurations) {
		final Configuration configuration = new Configuration();
		Arrays.stream(configurations).forEach(configuration::merge);
		return configuration;
	}

    private final Try<String, Optional<ContextOptions>> contextOptionsLookup =
            Try.<String, ContextOptions>evaluate(ctx -> MAPPER.readValue(MAPPER.writeValueAsString(_contextOptions.get(ctx)), ContextOptions.class))
                    .wrapWithOptional();

    public Optional<ContextOptions> getContextOptions(String name) {
        //TODO: toString ContextOptions
        return contextOptionsLookup.apply(name);
    }

	/**
	 * Provides a {@link Context} by it's name.
	 *
	 * @param name the context name
	 * @param <P>  The getType of payload dealt newInstance
	 * @param <R> The expected type result
	 * @return the configured context
	 */
	@SuppressWarnings("unchecked")
	public <P, R> Optional<Context<P, R>> getContext(String name) {
		if (!_contextOptions.containsKey(name)) {
			return Optional.empty();
		}
		if (!_cachedContexts.containsKey(name)) {
            _cachedContexts.putIfAbsent(name,
                    ContextBuilder.newInstance(name, this).get());
		}
		return Optional.ofNullable((Context<P, R>) _cachedContexts.get(name));
	}

    @SuppressWarnings("unchecked")
	public <P, R> Optional<Context<P, R>> updateContext(String name, ContextOptions options) {
		//TODO: toString ContextOptions
        _contextOptions.put(name, options);
        _cachedContexts.put(name,
                ContextBuilder.newInstance(name, this).get());
        return Optional.of((Context<P, R>) _cachedContexts.get(name));
	}

    public <P, R> Optional<Context<P, R>> updateContext(String name, Consumer<ContextOptions> decorator) {
        final Optional<ContextOptions> contextOptions = getContextOptions(name);
        if (contextOptions.isPresent()) {
            decorator.accept(contextOptions.get());
            return updateContext(name, contextOptions.get());
        }
        return Optional.empty();
    }

}