/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.configuration;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import lombok.ToString;

/**
 * @author Varun Anand
 * @since 1.0
 */
@ToString
public class Config {

	private final Map<String, Object> values;

	public Config() {
		this.values = new HashMap<>();
	}

	public Config(Map<String, ?> config) {
		this.values = Collections.unmodifiableMap(config);
	}

	public Config(Config config) {
		this(config.values);
	}

	public <T> T get(String key) {
		return (T) values.get(key);
	}

	public boolean contains(String key) {
		return values.containsKey(key);
	}

	public Map<String, Object> getAll() {
		return values;
	}

	public <T> T getOrDefault(String key, T defaultValue) {
		T value = get(key);
		if (null == value) {
			value = defaultValue;
		}
		return value;
	}

	public static Config from(Map<String, Object> values) {
		return new Config(values);
	}
}
