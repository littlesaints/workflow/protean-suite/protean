/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.analytics;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

public final class AggregateStatsListener<E> implements StatsListener<E> {

    private final Collection<StatsListener<E>> listeners;

    public AggregateStatsListener(StatsListener<E>... listeners) {
        this(Arrays.asList(listeners));
    }

    public AggregateStatsListener(Collection<StatsListener<E>> listeners) {
        this.listeners = new CopyOnWriteArrayList<>(listeners);
    }

    public void registerListener(StatsListener<E> listener) {
        this.listeners.add(listener);
    }

    @Override
    public void eventProcessed(String context, E event, String name, long startMillis, long endMillis) {
        listeners.forEach(l -> l.eventProcessed(context, event, name, startMillis, endMillis));
    }
}
