/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.exchanges.executor;

import io.gitlab.littlesaints.protean.Constants;
import io.gitlab.littlesaints.protean.configuration.Config;
import io.gitlab.littlesaints.protean.exchanges.MessageExchange;
import io.gitlab.littlesaints.protean.resources.Registrar;

/**
 * This executor performs ordered executions.
 * The order of execution for submitted objects is determined by a orderResolver. This orderResolver is provided during the instantiation of this executor.
 *
 * @author Varun Anand
 * @since 1.0
 */
public abstract class OrderedExecutionExchange {

	public static final String TYPE = "protean::exchange::execution::ordered";

	public static class _Registrar implements Registrar {
		static {
			MessageExchange.REGISTRY.register(TYPE, OrderedExecutionExchange::newInstance);
		}

		@Override
		public String toString() {
			return "OrderedExecutionExchange::Registrar";
		}
	}

	private static <E> MessageExchange<E> newInstance(final int capacity, final boolean isMultiProducer, final Config config) {
		final MessageExchange<E> executionExchange;
		if (isMultiProducer) {
			executionExchange = new OrderedExecutionMultiExchange<>(config.getOrDefault(Constants.NAME, System.currentTimeMillis() + "OEME"), config.<Integer>getOrDefault(Constants.CONCURRENCY, Runtime.getRuntime().availableProcessors()), capacity,
				config.get(Constants.Exchange.EXCHANGE_SUPPLIER), config.get(Constants.Exchange.PROCESSOR_SUPPLIER), config.get(Constants.Exchange.ORDER_RESOLVER), config.getOrDefault("lazyRoutes", true));
		} else {
			executionExchange = new OrderedExecutionSingleExchange<>(config.getOrDefault(Constants.NAME, System.currentTimeMillis() + "OESE"), config.<Integer>getOrDefault(Constants.CONCURRENCY, Runtime.getRuntime().availableProcessors()), capacity,
				config.get(Constants.Exchange.EXCHANGE_SUPPLIER), config.get(Constants.Exchange.PROCESSOR_SUPPLIER), config.get(Constants.Exchange.ORDER_RESOLVER));
		}
		return executionExchange;
	}

}
