package io.gitlab.littlesaints.protean.resources;

import io.gitlab.littlesaints.protean.configuration.Config;
import io.gitlab.littlesaints.protean.event.State;

import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Function defined in workflow yaml/json.
 * @param <E> event
 */
interface PFunction<E> extends Supplier<Function<E, State>> {}

/**
 * Function that's instantiated using a configuration {@link Config}, defined in workflow yaml/json.
 * @param <E> event
 */
interface CFunction<E> extends Function<Config, Function<E, State>> {}

/**
 * Function that's instantiated using a configuration {@link Config} and tool config, defined in workflow yaml/json.
 * @param <E> event
 * @param <T> tools
 */
interface TFunction<E, T> extends Supplier<BiFunction<E, T, State>> {}

/**
 * Function that's instantiated using a a configuration {@link Config} and tool config, defined in workflow yaml/json.
 * @param <E> event
 * @param <T> tools
 */
interface CTFunction<E, T> extends Function<Config, BiFunction<E, T, State>> {}

/**
 * Function that's instantiated using other workflow endpoints, defined in workflow yaml/json.
 * @param <E> event
 */
interface NFunction<E> extends Function<Map<String, Function<?, ?>>, Function<E, State>> {}

/**
 * Function that's instantiated using a configuration {@link Config} and other workflow endpoints, defined in workflow yaml/json.
 * @param <E> event
 */
interface CNFunction<E> extends BiFunction<Config, Map<String, Function<?, ?>>, Function<E, State>> {}

/**
 * Function that's instantiated using tools config and other workflow endpoints, defined in workflow yaml/json.
 * @param <E> event
 * @param <T> tools
 */
interface NTFunction<E, T> extends Function<Map<String, Function<?, ?>>, BiFunction<E, T, State>> {}

/**
 * Function that's instantiated using a configuration {@link Config}, tools config and other workflow endpoints, defined in workflow yaml/json.
 * @param <E> event
 * @param <T> tools
 */
interface CNTFunction<E, T> extends BiFunction<Config, Map<String, Function<?, ?>>, BiFunction<E, T, State>> {}