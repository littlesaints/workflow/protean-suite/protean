/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.exchanges.blocking;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.function.Predicate;

import io.gitlab.littlesaints.protean.exchanges.AbstractMessageExchange;

/**
 * Abstract class for implementing {@link BlockingQueue} based exchanges.
 *
 * @param <Q> the implementation of BlockingQueue to use
 * @param <E> the getType of element
 */
abstract class AbstractBlockingExchange<Q extends BlockingQueue<E>, E> extends AbstractMessageExchange<E> {

	private final Reader<E> reader;

	final Q queue;

	//TODO: implement wait strategy??
	AbstractBlockingExchange(Q queue) {
		this.reader = new _Reader(this.queue = queue);
	}

	private class _Reader implements Reader<E> {

		private final BlockingQueue<E> queue;

		_Reader(BlockingQueue<E> queue) {
			this.queue = queue;
		}

		@Override
		public E poll() {
			return queue.poll();
		}

		@Override
		public List<E> poll(int min, int max, int maxAttempts) {
			List<E> batch = null;
			E e;
			for (int attempts = 0; continueProcessing.get() && attempts < maxAttempts; Thread.yield()) {
				if ((e = queue.poll()) == null) {
					if (batch != null && batch.size() >= min) {
						break;
					}
					attempts++;
				} else {
					if (batch == null) {
						batch = new ArrayList<>(min);
					}
					batch.add(e);
					if (batch.size() == max) {
						break;
					}
					attempts = 0;
				}
			}
			return batch == null ? Collections.emptyList() : batch;
		}

		@Override
		public List<E> poll(int max) {
			List<E> batch = null;
			E e;
			for (; continueProcessing.get(); Thread.yield()) {
				if ((e = queue.poll()) == null) {
					break;
				} else {
					if (batch == null) {
						batch = new ArrayList<>();
					}
					batch.add(e);
					if (batch.size() == max) {
						break;
					}
				}
			}
			return batch == null ? Collections.emptyList() : batch;
		}

		@Override
		public boolean poll(int maxAttempts, Predicate<? super E> consumer) {
			int attempts = 0;
			E e;
			for (; continueProcessing.get() && attempts < maxAttempts; Thread.yield()) {
				if ((e = queue.poll()) == null) {
					attempts++;
				} else {
					if (!consumer.test(e)) {
						break;
					}
					attempts = 0;
				}
			}
			return continueProcessing.get() && attempts == maxAttempts;
		}

		@Override
		public E take() {
			while (true) {
				try {
					//TODO: test shutdown. check if executor terminates
					return queue.take();
				} catch (InterruptedException x) {
//					x.printStackTrace();
				}
			}
		}

		@Override
		public List<E> take(int min, int max) {
			List<E> batch = null;
			E e;
			for (; continueProcessing.get(); Thread.yield()) {
				if ((e = queue.poll()) == null) {
					if (batch != null && batch.size() >= min) {
						break;
					}
				} else {
					if (batch == null) {
						batch = new ArrayList<>(min);
					}
					batch.add(e);
					if (batch.size() == max) {
						break;
					}
				}
			}
			return batch == null ? Collections.emptyList() : batch;
		}

		@Override
		public List<E> take(int min) {
			List<E> batch = null;
			E e;
			for (; continueProcessing.get(); Thread.yield()) {
				if ((e = queue.poll()) == null) {
					if (batch != null && batch.size() >= min) {
						break;
					}
				} else {
					if (batch == null) {
						batch = new ArrayList<>(min);
					}
					batch.add(e);
				}
			}
			return batch == null ? Collections.emptyList() : batch;
		}

		@Override
		public void take(Predicate<? super E> consumer) {
			for (E e; (e = take()) != null && consumer.test(e); ) ;
		}

	}

	@Override
	public Reader<E> getReader() {
		return reader;
	}


	@Override
	public boolean isEmpty() {
		return queue.isEmpty();
	}
}
