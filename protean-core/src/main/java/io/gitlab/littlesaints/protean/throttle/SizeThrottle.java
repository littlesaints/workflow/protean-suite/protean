/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.throttle;

import java.util.concurrent.Semaphore;
import java.util.function.Consumer;

import io.gitlab.littlesaints.protean.configuration.Config;
import io.gitlab.littlesaints.protean.configuration.Defaults;

/**
 * A size based {@link Throttle}. It limits the maximum number of messages in the processing workflow / pipeline.
 *
 * @author Varun Anand
 * @since 1.0
 */
public class SizeThrottle implements Throttle {

	public static final String TYPE = "protean::throttle::size";

	public static class Registrar implements io.gitlab.littlesaints.protean.resources.Registrar {
		public Registrar() {
			REGISTRY.register(TYPE, SizeThrottle::new);
		}

		@Override
		public String toString() {
			return "SizeThrottle::Registrar";
		}
	}

	private final Semaphore permits;

	private final Consumer<Boolean> listener;

	interface Keys {
		String THRESHOLD = "threshold";
	}

	private SizeThrottle(Config config, Consumer<Boolean> listener) {
		final int threshold = config.getOrDefault(Keys.THRESHOLD, Defaults.BUFFER);
		this.permits = new Semaphore(threshold);
		//TODO implement listener
		this.listener = listener;
	}

	@Override
	public void acquire() {
		while (true) {
			try {
				permits.acquire();
				break;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void release() {
		permits.release();
	}

	@Override
	public boolean isActive() {
		return permits.availablePermits() > 0;
	}
}
