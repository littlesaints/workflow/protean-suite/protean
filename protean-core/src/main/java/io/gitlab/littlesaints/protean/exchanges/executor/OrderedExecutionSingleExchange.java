/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.exchanges.executor;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;
import java.util.stream.IntStream;

import io.gitlab.littlesaints.protean.Processor;
import io.gitlab.littlesaints.protean.exchanges.MessageExchange;

/**
 * This executor performs ordered executions.
 * The order of execution for submitted objects is determined by a orderResolver. This orderResolver is provided during the instantiation of this executor.
 *
 * @param <E> The getType of objects to execute upon.
 * @author Varun Anand
 * @since 1.0
 */
class OrderedExecutionSingleExchange<E> extends AbstractExecutionExchange<E> {

    public static final String TYPE = "exchange::execution::java::ordered::single";

    private static final String nameSuffix = "::OrderedExecutionExchange-single";

    private final Reader<E> reader;

    private final Thread[] workers;
    private final Runnable[] onCloseOps;

    private final int bufferSize;

    private final ToIntFunction<E> orderResolver;

    private final Reader<E>[] outputExchangeReaders;
    private final Writer<E>[] outputExchangeWriters;

    private final Reader<E>[] inputExchangeReaders;
    private final Writer<E>[] inputExchangeWriters;

    private final AtomicReferenceArray<MessageExchange<E>> inputExchanges;
    private final AtomicReferenceArray<MessageExchange<E>> outputExchanges;

    /**
     * Construct an executor
     *
     * @param name              the name of this exchange. Pass null, if no specific value is needed.
     * @param concurrency       the 'fixed' number of threads to use
     * @param capacity          the buffer size of each thread's message exchange
     * @param exchangeSupplier  the supplier function for creating a message exchange
     * @param processorSupplier the action to perform on a submitted object/event
     * @param orderResolver     the function to resolve the order of execution for submitted objects/events.
     */
    OrderedExecutionSingleExchange(String name, int concurrency, int capacity, IntFunction<MessageExchange<E>> exchangeSupplier, Supplier<Processor<E>> processorSupplier,
                                   ToIntFunction<E> orderResolver) {

        super(name + nameSuffix, processorSupplier, concurrency, exchangeSupplier);
        this.bufferSize = capacity;
        this.orderResolver = orderResolver;
        inputExchanges = new AtomicReferenceArray<>(concurrency);
        outputExchanges = new AtomicReferenceArray<>(concurrency);

        reader = new _Reader();

        workers = new Thread[concurrency];
        onCloseOps = new Runnable[concurrency];

        outputExchangeReaders = new Reader[concurrency];
        outputExchangeWriters = new Writer[concurrency];
        inputExchangeReaders = new Reader[concurrency];
        inputExchangeWriters = new Writer[concurrency];
    }

    private class _Reader extends _ExecutionExchangeReader {
        @Override
        Reader<E> getOutputExchangeReader(int idx) {
            return outputExchangeReaders[idx];
        }
    }

    private class _Writer implements Writer<E> {
        @Override
        public boolean put(final E e) {
            int route = orderResolver.applyAsInt(e) & (concurrency - 1);
            initRoute(route);
            initRouteProcessor(route);
            inputExchangeWriters[route].put(e);
            return true;
        }
    }

    private void initRoute(int route) {
        if (outputExchangeReaders[route] == null) {
            MessageExchange<E> inputExchange = exchangeSupplier.apply(bufferSize);
            inputExchangeReaders[route] = inputExchange.getReader();
            inputExchangeWriters[route] = inputExchange.newWriter();

            MessageExchange<E> outputExchange = exchangeSupplier.apply(bufferSize);
            outputExchangeReaders[route] = outputExchange.getReader();
            outputExchangeWriters[route] = outputExchange.newWriter();

            inputExchanges.set(route, inputExchange);
            outputExchanges.set(route, outputExchange);
        }
    }

    private void initRouteProcessor(int route) {
        Thread worker = workers[route];
        if (worker == null || !worker.isAlive()) {
            if (worker != null) {
                onCloseOps[route].run();
            }
            final Processor<E> processor = processorSupplier.get();
            final Consumer<E> action = processor.getFunction()::apply;
            workers[route] = new Thread(() ->
                    inputExchangeReaders[route].take(e -> {
                        action.accept(e);
                        outputExchangeWriters[route].put(e);
                        return true;
                    }), getName() + "-worker-" + route);
            workers[route].start();
            onCloseOps[route] = processor::close;
        }
    }

    @Override
    public Reader<E> getReader() {
        return reader;
    }

    @Override
    public Writer<E> newWriter() {
        return new _Writer();
    }

    @Override
    public void shutdown() {
//		while (!isEmpty()) {
//			Thread.yield();
//		}
        IntStream.range(0, concurrency)
                .boxed()
                .map(inputExchanges::get)
                .filter(Objects::nonNull)
                .forEach(MessageExchange::shutdown);
        IntStream.range(0, concurrency)
                .boxed()
                .map(outputExchanges::get)
                .filter(Objects::nonNull)
                .forEach(MessageExchange::shutdown);
        Arrays.stream(workers).filter(Objects::nonNull).filter(Thread::isAlive).forEach(Thread::interrupt);
        Arrays.stream(onCloseOps).filter(Objects::nonNull).forEach(Runnable::run);
    }

    @Override
    public boolean isEmpty() {
        return IntStream.range(0, concurrency)
                .boxed()
                .map(inputExchanges::get)
                .filter(Objects::nonNull)
                .map(MessageExchange::isEmpty)
                .reduce((e1, e2) -> e1 && e2)
                .orElse(Boolean.FALSE);
    }
}
