/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.event;

import io.gitlab.littlesaints.protean.configuration.Config;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.function.Supplier;

/**
 * An event to be used when it's being accessed by a single thread.
 * This class is NOT thread-safe.
 */
public class SingleThreadEvent implements SimpleEvent {

    private final HashMap<String, Object> props;

    private final Config confProps;

    @Getter
    @Setter
    private State state;

    private Long identifier;

    @Getter
    private long startEpochMillis;

    private Object payload;

    public SingleThreadEvent(Config config) {
        props = new HashMap<>();
        confProps = new Config(config);
    }

    public void init(Supplier<Long> idGenerator) {
        state = State.SUCCESS;
        identifier = idGenerator.get();
        startEpochMillis = System.currentTimeMillis();
    }

    public long getIdentifier() {
        return identifier;
    }

    @SuppressWarnings("unchecked")
    public <T> T getProp(String key) {
        return (T) props.get(key);
    }

    @SuppressWarnings("unchecked")
    public <T> T setProp(String key, T value) {
        return (T) props.put(key, value);
    }

    public boolean containsProp(String key) {
        return props.containsKey(key);
    }

    @SuppressWarnings("unchecked")
    public <T> T getConfig(String key) {
        return (T) confProps.get(key);
    }

    public <T> T getPayload() {
        return (T) payload;
    }

    public <T> void setPayload(T payload) {
        this.payload = payload;
    }
}