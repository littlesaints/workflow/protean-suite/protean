/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.context;

import io.gitlab.littlesaints.protean.Constants;
import io.gitlab.littlesaints.protean.Context;
import io.gitlab.littlesaints.protean.EventExecutor;
import io.gitlab.littlesaints.protean.Graph;
import io.gitlab.littlesaints.protean.analytics.AggregateStatsListener;
import io.gitlab.littlesaints.protean.analytics.StatsListener;
import io.gitlab.littlesaints.protean.analytics.StatsLogger;
import io.gitlab.littlesaints.protean.configuration.*;
import io.gitlab.littlesaints.protean.event.EventAccessor;
import io.gitlab.littlesaints.protean.event.EventCache;
import io.gitlab.littlesaints.protean.executors.sync.SyncExecutor;
import io.gitlab.littlesaints.protean.resources.ProteanResources;
import io.gitlab.littlesaints.protean.resources.ResourceLocator;
import io.gitlab.littlesaints.protean.throttle.Throttle;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * A builder for {@link Context}.
 * An instance of this class can be created using {@link #newInstance(String, Configuration)}.
 * The class provides various methods to build a {@link Context}.
 * <p>
 * This class is NOT thread-safe.
 * </p>
 *
 * @param <P> the getType of payload to submit for execution.
 * @author Varun Anand
 * @see Context
 * @since 0.1
 */
public final class ContextBuilder<E, P, R> implements Supplier<Context<P, R>> {

    private final Configuration configuration;

    private final ResourceLocator locator;

    private final String contextName;

    private final ContextOptions contextOptions;

    private AggregateStatsListener<E> statsListener;

    private Throttle throttle;

    private ContextBuilder(String contextName, Configuration configuration) {
        this.contextName = contextName;
        this.configuration = configuration;
        this.locator = Configuration.LOCATOR;
        this.contextOptions = configuration.getContextOptions(contextName).orElseThrow(IllegalArgumentException::new);
    }

    public static <E, P, R> ContextBuilder<E, P, R> newInstance(String contextName, Configuration configuration) {
        return new ContextBuilder<>(contextName, configuration);
    }

    public EventExecutor<P, R> buildExecutor() {

        final EventAccessor<E, P> eventAccessor = buildAccessor();

        Optional<Consumer<E>> ackListenerRef = Optional.empty();

        final Optional<EventCache<E>> eventCacheRef = buildEventCache(eventAccessor);
        final Function<Config, E> constructor;
        if (eventCacheRef.isPresent()) {
            final EventCache<E> eventCache = eventCacheRef.get();
            constructor = c -> eventCache.get();
            ackListenerRef = Optional.of(eventCache::put);
        } else {
            if (SyncExecutor.TYPE.equals(contextOptions.getExecutor().getType().getSupplier())) {
                contextOptions.getEvent().getType().setSupplier(ProteanResources.SINGLE_THREAD_EVENT_SUPPLIER);
            }
            constructor = buildEventSupplier();
        }
        Objects.requireNonNull(constructor, "constructor cannot be null !!");

        this.throttle = buildThrottle().orElse(null);
        if (throttle != null) {
            final Consumer<E> c = e -> throttle.release();
            if (ackListenerRef.isPresent()) {
                ackListenerRef = ackListenerRef.map(ackL -> ackL.andThen(c));
            } else {
                ackListenerRef = Optional.of(c);
            }
        }

        final Optional<Function<E, String>> refIdResolverRef = locator.lookup(contextOptions.getEvent().getReferenceIdResolver());
        final Function<E, String> idResolver;
        if (refIdResolverRef.isPresent()) {
            final Function<E, String> refIdResolver = refIdResolverRef.get();
            idResolver = e -> refIdResolver.apply(e) + "|" + eventAccessor.getIdentifier(e);
        } else {
            idResolver = e -> "" + eventAccessor.getIdentifier(e);
        }

        this.statsListener = buildAggregateStatsListener(idResolver);
        final boolean doStatistics = contextOptions.getAnalytics().isStatistics();
        if (doStatistics) {
            final Consumer<E> c = e -> statsListener.eventProcessed(contextName, e, Constants.Statistics.E2E,
                    eventAccessor.getStartEpochMillis(e), System.currentTimeMillis());
            if (ackListenerRef.isPresent()) {
                ackListenerRef = ackListenerRef.map(ackL -> ackL.andThen(c));
            } else {
                ackListenerRef = Optional.of(c);
            }
        }

        final Function<E, R> resultResolver = locator.lookupOrThrow(contextOptions.getResultMapper(), "Result Evaluator");

        final EventExecutor<P, R> executor = buildExecutor(() -> constructor.apply(new Config(contextOptions.getEvent().getType().getConfig())),
                eventAccessor, idResolver, ackListenerRef.orElse(e -> {
                }), doStatistics ? statsListener : null, resultResolver);

        Objects.requireNonNull(executor, "executor cannot be null !!");

        return executor;
    }

    public Context<P, R> get() {
        return Context.REGISTRY.lookupOrThrow(contextOptions.getProvider(), "Context Provider")
                .newInstance(contextName, configuration, statsListener, buildExecutor(), throttle);
    }

    private Optional<EventCache<E>> buildEventCache(EventAccessor<E, P> eventAccessor) {
        final CacheOptions cacheOptions = contextOptions.getEvent().getCache();
        if (cacheOptions == null) {
            return Optional.empty();
        }
        return Optional.of(EventCache.REGISTRY.lookupOrThrow(cacheOptions.getType().getSupplier(), "EventCache")
                .newInstance(new Config(cacheOptions.getType().getConfig()),
                        cacheOptions.getSize() == 0 ? contextOptions.getExecutor().getBuffer() : cacheOptions.getSize(),
                        p -> {
                            Props props = new Props();
                            props.putAll(p);
                            props.putAll(contextOptions.getEvent().getType().getConfig());
                            return buildEventSupplier().apply(new Config(props));
                        }, eventAccessor));
    }

    private EventAccessor<E, P> buildAccessor() {
        return locator.lookupOrThrow(contextOptions.getEvent().getAccessor(), "Accessor");
    }

    private EventExecutor<P, R> buildExecutor(Supplier<E> constructor, EventAccessor<E, P> eventAccessor, Function<E, String> idResolver,
                                              Consumer<E> ackListener, StatsListener<E> statsListener,
                                              Function<E, R> resultResolver) {
        final Graph<E> graph = Graph.<E>configure().configuration(configuration)
                .locator(locator)
                .options(contextOptions.getWorkflow())
                .get();
        Objects.requireNonNull(graph, "workflow cannot be null !!");
        final BiConsumer<E, P> initializer = buildInitializer(eventAccessor);
        final ExecutorOptions executorOptions = contextOptions.getExecutor();
        return EventExecutor.REGISTRY.lookupOrThrow(executorOptions.getType().getSupplier(), "Executor")
                .newInstance(contextName, executorOptions, constructor, graph, initializer, ackListener, eventAccessor,
                        contextOptions.getAnalytics().isStatistics() ? statsListener : null, contextOptions.isCleanShutdown(), resultResolver);
    }

    private BiConsumer<E, P> buildInitializer(EventAccessor<E, P> eventAccessor) {
        Objects.requireNonNull(eventAccessor, "initializer cannot be null !!");
        return eventAccessor::initialize;
    }

    private AggregateStatsListener<E> buildAggregateStatsListener(Function<E, String> idResolver) {
        final List<StatsListener<E>> listeners = contextOptions.getAnalytics().getListeners().stream()
                .map(this::<StatsListener<E>>lookupGenericOptions)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
        listeners.add(new StatsLogger<>(idResolver));
        return new AggregateStatsListener<>(listeners);
    }

    private Function<Config, E> buildEventSupplier() {
        return locator.lookupOrThrow(contextOptions.getEvent().getType().getSupplier(), "EventSupplier");
    }

    private Optional<Throttle> buildThrottle() {
        final Throttle throttle;
        {
            if (contextOptions.getThrottle() == null) {
                throttle = null;
            } else {
                ThrottleOptions o = contextOptions.getThrottle();
                throttle = Throttle.REGISTRY.lookupOrThrow(o.getType().getSupplier(), "Throttle")
                        .newInstance(new Config(o.getType().getConfig()),
                                o.getListeners().stream()
                                        .map(this::<Consumer<Boolean>>lookupGenericOptions)
                                        .filter(Optional::isPresent)
                                        .map(Optional::get)
                                        .reduce(Consumer::andThen)
                                        .orElse(b -> {
                                        }));
            }
        }
        return Optional.ofNullable(throttle);
    }

    private <T> Optional<T> lookupGenericOptions(GenericOptions options) {
        if (options.getConfig().isEmpty()) {
            return locator.<Supplier<T>>lookup(options.getSupplier()).map(Supplier::get);
        } else {
            return locator.<Function<Config, T>>lookup(options.getSupplier())
                    .map(f -> f.apply(Config.from(options.getConfig())));
        }
    }

}
