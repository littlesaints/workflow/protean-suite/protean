/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.resources;

import io.gitlab.littlesaints.protean.configuration.Configuration;

import java.util.Optional;

/**
 * It's used to lookup named resources.
 * Applications can define their functions in a repository of their choice (e.g. Spring) and define a corresponding ResourceLocator to facilitate their lookup.
 * These functions can be defined in a supported format loaded using the {@link Configuration}'s loadXX methods.
 */
public interface ResourceLocator {

	/**
	 * The repository of all available implementations.
	 */
	Registry<ResourceLocator> REGISTRY = new Registry<>();

	/**
	 * Lookup a resource by it's name
	 *
	 * @param name the name of the resource to lookup
	 * @param <T>  the getType of resource
	 * @return the resource, if available, or <i>null</i> otherwise
	 */
	default <T> Optional<T> lookup(String name) {
		if (name == null) {
			return Optional.empty();
		}
		return doLookup(name);
	}

	<T> Optional<T> doLookup(String name);

	default <T> T lookupOrThrow(String name, String type) {
		Optional<T> resourceRef = lookup(name);
		return resourceRef.orElseThrow(() -> new RuntimeException("Unable to lookup " + type + " newInstance name " + name + " !!"));
	}

	default <T> Optional<T> optionalLookupOrThrow(String name, String type) {
		if (name == null) {
			return Optional.empty();
		}
		return Optional.of(lookupOrThrow(name, type));
	}

	default <T> T lookup(String name, T defaultValue) {
		Optional<T> op = lookup(name);
		return op.orElse(defaultValue);
	}

}
