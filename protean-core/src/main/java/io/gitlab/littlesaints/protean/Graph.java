/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;

import io.gitlab.littlesaints.protean.configuration.Configuration;
import io.gitlab.littlesaints.protean.configuration.GraphOptions;
import io.gitlab.littlesaints.protean.resources.ResourceLocator;

import lombok.Builder;
import lombok.NonNull;
import lombok.Singular;
import lombok.Value;

/**
 * Defines a complete processing workflow / pipeline. It includes all the processing {@link Node}s and their inter-dependencies.
 *
 * @see Node
 * @see Context
 */
@Value
public final class Graph<E> {

    private final Collection <Node <E>> nodes;

    private final Map <String, Collection <String>> dependencies;

    private final Collection <String> roots;

    private final Collection <String> leaves;

    private final Map <String, Collection <String>> hierarchy;

    private final Map <String, Node <E>> nodesMap;

    public static <E> Graph<E> of(Collection <Node<E>> nodes) {
        return new Graph<>(nodes, Collections.emptyMap());
    }

    @Builder
    private Graph(@Singular @NonNull Collection<Node <E>> nodes, @Singular Map<String, Collection <String>> dependencies) {
        this.nodes = Collections.unmodifiableCollection(nodes);
        this.dependencies = dependencies == null ? Collections.emptyMap() : Collections.unmodifiableMap(dependencies);
        this.roots = Collections.unmodifiableCollection(evaluateRoots(this.nodes, this.dependencies));
        this.hierarchy = Collections.unmodifiableMap(evaluateHierarchy(this.dependencies));
        this.leaves = Collections.unmodifiableCollection(evaluateLeaves(this.nodes, this.hierarchy));
        this.nodesMap = Collections.unmodifiableMap(evaluateNodesMap(this.nodes));
        validate();
    }

    @Builder(builderClassName = "GraphConfigurer", builderMethodName = "configure", buildMethodName = "get")
    private static <E> Graph <E> from(@NonNull Configuration configuration, @NonNull ResourceLocator locator, @NonNull GraphOptions options/*, @NonNull EventAccessor accessor*/) {
        GraphBuilder <E> builder = Graph.builder();
        options.getStages().stream()
            .map(o -> Node.<E>configure().configuration(configuration).locator(locator).options(o)/*.accessor(accessor)*/.get())
            .forEach(builder::node);
        builder.dependencies(options.getDependencies());
        return builder.build();
    }

    private void validate() {
        if (nodes.isEmpty()) {
            throw new RuntimeException("nodes not configured !!");
        }

        Objects.requireNonNull(roots, "roots cannot be null !!");
        if (roots.isEmpty()) {
            throw new RuntimeException("roots not configured !!");
        }

        Objects.requireNonNull(leaves, "leaves cannot be null !!");
        if (leaves.isEmpty()) {
            throw new RuntimeException("leaves not configured !!");
        }

        Objects.requireNonNull(nodesMap, "nodesMap cannot be null !!");
        if (nodesMap.isEmpty()) {
            throw new RuntimeException("nodesMap not configured !!");
        }

        if (new HashSet <>(nodes).size() != nodes.size()) {
            throw new RuntimeException("Found multiple nodes newInstance the same name !!");
        }
    }

    private Map <String, Collection <String>> evaluateHierarchy(Map <String, Collection <String>> dependencies) {
        final Map <String, Collection <String>> hierarchy = new HashMap <>(dependencies.size());
        dependencies.forEach((s, d) -> {
            Collection <String> list = Collections.singleton(s);
            d.forEach(v ->
                hierarchy.merge(v, new ArrayList <>(list), (o, n) -> {
                    o.addAll(n);
                    return o;
                })
            );
        });
        return hierarchy;
    }

    private Collection <String> evaluateRoots(Collection <Node <E>> nodes, Map <String, Collection <String>> dependencies) {
        final Collection <String> roots = new ArrayList <>(1);
        for (Node <E> node : nodes) {
            if (!dependencies.containsKey(node.getName())) {
                roots.add(node.getName());
            }
        }
        return roots;
    }

    private Collection <String> evaluateLeaves(Collection <Node <E>> nodes, Map <String, Collection <String>> hierarchy) {
        final Collection <String> leaves = new ArrayList <>(1);
        for (Node <E> node : nodes) {
            if (!hierarchy.containsKey(node.getName())) {
                leaves.add(node.getName());
            }
        }
        return leaves;
    }

    private Map <String, Node <E>> evaluateNodesMap(Collection <Node <E>> nodes) {
        final Map <String, Node <E>> nodesMap = new HashMap <>(nodes.size());
        for (Node <E> node : nodes) {
            nodesMap.put(node.getName(), node);
        }
        return nodesMap;
    }

}