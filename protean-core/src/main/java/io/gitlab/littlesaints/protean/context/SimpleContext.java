/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.context;

import io.gitlab.littlesaints.protean.Context;
import io.gitlab.littlesaints.protean.EventExecutor;
import io.gitlab.littlesaints.protean.analytics.AggregateStatsListener;
import io.gitlab.littlesaints.protean.configuration.Configuration;
import io.gitlab.littlesaints.protean.resources.Registrar;
import io.gitlab.littlesaints.protean.throttle.Throttle;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.Objects;
import java.util.function.Function;

/**
 * This represents a complete execution workflow / pipeline.
 * It should be used by the application to retrieve an endpoint using {@link #getEndpoint()}, to submit payloads for execution.
 * <p>
 * It can be built using a {@link Configuration} or a {@link ContextBuilder}.
 *
 * @param <P> the getType of payload that'll be submitted.
 * @param <R> the getType of result that will be returned.
 * @author Varun Anand
 * @since 1.0
 */
@Log4j2
public final class SimpleContext<P, R> implements Context<P, R> {

    public static final String TYPE = "protean::context::simple";

    public static class _Registrar implements Registrar {
        static {
            REGISTRY.register(TYPE, SimpleContext::new);
        }

        @Override
        public String toString() {
            return "SimpleContext::Registrar";
        }
    }

    @Getter
    private final Function<P, R> endpoint;

    private final Runnable shutdownAction;

    private <E> SimpleContext(String name, Configuration configuration, final AggregateStatsListener<E> statsListener,
                             EventExecutor<P, R> executor, Throttle throttle) {
        Objects.requireNonNull(executor, "executor cannot be null !!");
        if (throttle == null) {
            this.endpoint = executor::execute;
        } else {
            this.endpoint = p -> {
                throttle.acquire();
                return executor.execute(p);
            };
        }
        shutdownAction = executor::shutdown;
    }

    public void shutdown() {
        shutdownAction.run();
    }

}
