/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean;

import io.gitlab.littlesaints.protean.configuration.Config;
import io.gitlab.littlesaints.protean.configuration.Configuration;
import io.gitlab.littlesaints.protean.configuration.ProcessorOptions;
import io.gitlab.littlesaints.protean.event.State;
import io.gitlab.littlesaints.protean.resources.ResourceLocator;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Log4j2
public class Processor<E> {

    @Getter
    @NonNull
    private final Function<E, State> function;

    @NonNull
    private final Runnable onClose;

    public static <E> Processor<E> of(Function<E, State> function, Runnable onClose) {
        return new Processor <>(function, onClose);
    }

    public static <E, T> Supplier<Processor<E>> supplier(Configuration configuration, ResourceLocator locator, ProcessorOptions options) {

        final Map<String, Function<?, ?>> endpoints = evaluateEndpoints(configuration, options);
        final Config config = evaluateConfiguration(options);

        final Optional <Supplier <T>> toolsSupplierOp = locator.optionalLookupOrThrow(options.getTools(), "tools");
        if (toolsSupplierOp.isPresent()) {

            final Optional <Consumer <T>> onCloseOp = locator.optionalLookupOrThrow(options.getOnClose(), "onClose");

            final BiFunction<E, T, State> function;
            if (config == null) {
                if (endpoints == null) {
                    final Supplier<BiFunction<E, T, State>> supplier = locator.lookupOrThrow(options.getSupplier(), "Resource");
                    function = supplier.get();
                } else {
                    final Function<Map<String, Function<?, ?>>, BiFunction<E, T, State>> supplier = locator.lookupOrThrow(options.getSupplier(), "Resource");
                    function = supplier.apply(endpoints);
                }
            } else {
                if (endpoints == null) {
                    final Function<Config, BiFunction<E, T, State>> supplier = locator.lookupOrThrow(options.getSupplier(), "Resource");
                    function = supplier.apply(config);
                } else {
                    final BiFunction<Config, Map<String, Function<?, ?>>, BiFunction<E, T, State>> supplier = locator.lookupOrThrow(options.getSupplier(), "Resource");
                    function = supplier.apply(config, endpoints);
                }
            }

            final Supplier <T> toolsSupplier = toolsSupplierOp.get();
            if (onCloseOp.isPresent()) {
                final Consumer <T> onClose = onCloseOp.get();
                return () -> {
                    final T tools = toolsSupplier.get();
                    return Processor.of(e -> function.apply(e, tools), () -> onClose.accept(tools));
                };
            } else {
                return () -> {
                    final T tools = toolsSupplier.get();
                    return Processor.of(e -> function.apply(e, tools), () -> {});
                };
            }

        } else {

            final Optional <Runnable> onCloseOp = locator.optionalLookupOrThrow(options.getOnClose(), "onClose");

            final Function<E, State> function;
            if (config == null) {
                if (endpoints == null) {
                    final Supplier<Function<E, State>> supplier = locator.lookupOrThrow(options.getSupplier(), "Resource");
                    function = supplier.get();
                } else {
                    final Function<Map<String, Function<?, ?>>, Function<E, State>> supplier = locator.lookupOrThrow(options.getSupplier(), "Resource");
                    function = supplier.apply(endpoints);
                }
            } else {
                if (endpoints == null) {
                    final Function<Config, Function<E, State>> supplier = locator.lookupOrThrow(options.getSupplier(), "Resource");
                    function = supplier.apply(config);
                } else {
                    final BiFunction<Config, Map<String, Function<?, ?>>, Function<E, State>> supplier = locator.lookupOrThrow(options.getSupplier(), "Resource");
                    function = supplier.apply(config, endpoints);
                }
            }

            if (onCloseOp.isPresent()) {
                final Runnable onClose = onCloseOp.get();
                return () -> Processor.of(function, onClose);
            } else {
                return () -> Processor.of(function, () -> {});
            }
        }
    }

    private static Config evaluateConfiguration(ProcessorOptions options) {
        final Config config;
        if (options.getConfig().isEmpty()) {
            config = null;
        } else {
            config = new Config(options.getConfig());
        }
        return config;
    }

    private static Map<String, Function<?, ?>> evaluateEndpoints(Configuration configuration, ProcessorOptions options) {
        final Map<String, Function<?, ?>> endpoints;
        if (options.getContexts().isEmpty()) {
            endpoints = null;
        } else {
            endpoints = new HashMap<>();
            options.getContexts().forEach(s -> {
                if (configuration.getContext(s).isPresent()) {
                    endpoints.put(s, configuration.getContext(s).get().getEndpoint());
                } else {
                    log.error("Referenced context {} not found !!", s);
                }
            });
        }
        return endpoints;
    }

    public static <E> Supplier<Processor<E>> compose(Supplier<Processor<E>> thisSupplier, Supplier<Processor<E>> thatSupplier) {
        return () -> {
            Processor<E> thisProcessor = thisSupplier.get();
            Processor<E> thatProcessor = thatSupplier.get();
            Function<E, State> compositeFx = e -> {
                State state;
                if ((state = thisProcessor.function.apply(e)) == State.SUCCESS) {
                    return thatProcessor.function.apply(e);
                } else {
                    return state;
                }
            };
            Runnable compositeOnClose = () -> {
                thisProcessor.close();
                thatProcessor.close();
            };
            return new Processor<>(compositeFx, compositeOnClose);
        };
    }

    public void close() {
        onClose.run();
    }

}
