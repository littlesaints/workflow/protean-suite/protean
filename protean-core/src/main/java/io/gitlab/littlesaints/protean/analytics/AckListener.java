/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.analytics;

import io.gitlab.littlesaints.protean.Constants;

import java.util.Objects;

/**
 * @author Varun Anand
 * @since 1.0
 */
public interface AckListener<E> extends StatsListener<E> {

	default void eventProcessed(String context, E event, String name, long startMillis, long endMillis) {
		if (Objects.equals(Constants.Statistics.E2E, name)) {
			eventAcknowledged(context, event, startMillis, endMillis);
		}
	}

	void eventAcknowledged(String context, E event, long startMillis, long endMillis);
}