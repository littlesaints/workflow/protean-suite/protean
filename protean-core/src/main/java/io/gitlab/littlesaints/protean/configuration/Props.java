/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.configuration;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Varun Anand
 * @since 1.0
 */
public class Props extends ConcurrentHashMap<String, Object> {

	public <T> T get(String key) {
		return (T) super.get(key);
	}

	public <T> T getOrDefault(String key, T defaultValue) {
		return (T) super.getOrDefault(key, defaultValue);
	}

	@Override
	public String toString() {
		return "Props{" + super.toString() + "}";
	}
}
