/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.configuration;

import io.gitlab.littlesaints.protean.context.SimpleContext;
import io.gitlab.littlesaints.protean.event.SimpleEventCache;
import io.gitlab.littlesaints.protean.exchanges.blocking.LinkedTransferExchange;
import io.gitlab.littlesaints.protean.executors.sync.SyncExecutor;
import io.gitlab.littlesaints.protean.resources.ProteanResources;
import io.gitlab.littlesaints.protean.throttle.SizeThrottle;

/**
 * @author Varun Anand
 * @since 1.0
 */
public interface Defaults {

	String EXECUTOR = SyncExecutor.TYPE;
	String EXCHANGE = LinkedTransferExchange.TYPE;
	int BUFFER = 65536;
	String EVENT_SUPPLIER = ProteanResources.THREAD_SAFE_EVENT_SUPPLIER;
	String EVENT_ACCESSOR = ProteanResources.SIMPLE_EVENT_ACCESSOR;
	String EVENT_CACHE = SimpleEventCache.TYPE;
	String THROTTLE = SizeThrottle.TYPE;
	String CONTEXT = SimpleContext.TYPE;
	String RESULT_MAPPER = ProteanResources.CONTEXT_RESULT_MAPPER;
}
