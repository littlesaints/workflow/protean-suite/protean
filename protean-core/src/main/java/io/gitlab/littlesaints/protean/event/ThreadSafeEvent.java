/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.event;

import io.gitlab.littlesaints.protean.configuration.Config;
import io.gitlab.littlesaints.protean.configuration.Props;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

/**
 * A {@link java.util.concurrent.ConcurrentHashMap} based implementation of an Event.
 */
public class ThreadSafeEvent implements SimpleEvent {

	private final Props props;

	private final Config confProps;

	private final AtomicReference<State> state;

	private final AtomicLong identifier;

	private final AtomicLong startEpochMillis;

	private final AtomicReference<Object> payload;

	public ThreadSafeEvent(Config config) {
		props = new Props();
		confProps = new Config(config);
		state = new AtomicReference<>(State.SUCCESS);
		identifier = new AtomicLong();
		startEpochMillis = new AtomicLong();
		payload = new AtomicReference<>();
	}

	public void init(Supplier<Long> idGenerator) {
		props.clear();
		state.set(State.SUCCESS);
		identifier.set(idGenerator.get());
		startEpochMillis.set(System.currentTimeMillis());
		payload.set(null);
	}

	public long getIdentifier() {
		return identifier.get();
	}

	@SuppressWarnings("unchecked")
	public <T> T getProp(String key) {
		return (T) props.get(key);
	}

	@SuppressWarnings("unchecked")
	public <T> T setProp(String key, T value) {
		return (T) props.put(key, value);
	}

	public boolean containsProp(String key) {
		return props.containsKey(key);
	}

	@SuppressWarnings("unchecked")
	public <T> T getConfig(String key) {
		return (T) confProps.get(key);
	}

	public State getState() {
		return state.get();
	}

	public void setState(State state) {
		this.state.set(state);
	}

	public long getStartEpochMillis() {
		return startEpochMillis.get();
	}

	public <T> T getPayload() {
		return (T) payload.get();
	}

	public <T> void setPayload(T payload) {
		this.payload.set(payload);
	}
}