/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.analytics;

import io.gitlab.littlesaints.protean.StructuredLoggers;
import lombok.extern.log4j.Log4j2;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author Varun Anand
 * @since 1.0
 */
@Log4j2
public class StatsLogger<E> implements StatsListener<E> {

    private final String eventProcessedLogTemplate;

    private final Function<E, String> idResolver;

    public StatsLogger(Function<E, String> idResolver) {
        this(StructuredLoggers.JSON, idResolver);
    }

    public StatsLogger(Supplier<StructuredLoggers.Format> formatSupplier, Function<E, String> idResolver) {
        eventProcessedLogTemplate = formatSupplier.get()
                .bind("metric", String.class)
                .bind("context", String.class)
                .bind("id", String.class)
                .bind("stage", String.class)
                .bind("start_timestamp", Long.class)
                .bind("end_timestamp", Long.class)
                .bind("time_taken_millis", Long.class)
                .build();
        this.idResolver = idResolver;
    }

    @Override
    public void eventProcessed(String context, E event, String name, long startMillis, long endMillis) {
        log.info(eventProcessedLogTemplate, "event_performance", context, idResolver.apply(event),
                name, startMillis, endMillis,endMillis - startMillis);
    }

}
