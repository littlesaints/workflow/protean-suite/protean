/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean;

import com.littlesaints.protean.functions.streams.Try;
import com.littlesaints.protean.functions.trial.FnTrial;
import com.littlesaints.protean.functions.trial.Strategy;
import io.gitlab.littlesaints.protean.analytics.StatsListener;
import io.gitlab.littlesaints.protean.configuration.ExecutorOptions;
import io.gitlab.littlesaints.protean.event.EventAccessor;
import io.gitlab.littlesaints.protean.event.State;
import io.gitlab.littlesaints.protean.resources.Registry;

import java.util.function.*;

/**
 * An interface for executors that common and run the execution workflow / pipeline as defined in a {@link Context}.
 *
 * @author Varun Anand
 * @since 1.0
 */
public interface EventExecutor<P, R> {

	/**
	 * Repository for all configured executors.
	 */
	Registry<Factory> REGISTRY = new Registry<>();

	/**
	 * Submit a payload for execution.
	 *
	 * @param payload     the object to execute on.
	 */
	R execute(final P payload);

	static <E> boolean filter(E e, boolean skippable, Predicate<E> filter, Function<E, State> stateResolver) {
		return filter(e, skippable, stateResolver) && filter(e, filter);
	}

	static <E> boolean filter(E e, boolean skippable, Function<E, State> stateResolver) {
		return stateResolver.apply(e) == State.SUCCESS || !skippable;
	}

	static <E> boolean filter(E e, Predicate<E> filter) {
		return filter == null || filter.test(e);
	}

	static <E> Function<E, State> newExecutionOp(Function<E, State> processor, BiFunction<E, Exception, State> exceptionHandler,
                                            Strategy retryStrategy) {
        final Function<E, State> op;
        if (exceptionHandler == null) {
            op = processor;
        } else {
            op = Try.evaluate(processor::apply).onException(exceptionHandler);
        }
        if (retryStrategy == null) {
            return op;
        } else {
            return FnTrial.of(retryStrategy.toBuilder().build(), op, s -> s != State.RETRY, s -> State.FAILURE);
        }
    }

	static <E> void process(Function<E, State> executionOp, E e,
                            Function<E, State> stateResolver, BiConsumer<E, State> stateRegistrar) {
	    State state = executionOp.apply(e);
		if (stateResolver.apply(e) == State.SUCCESS && state != State.SUCCESS) {
			stateRegistrar.accept(e, state);
		}
	}

	void shutdown();

	/**
	 * The factory method for all implementations.
	 * This method will be configured in {@link #REGISTRY} to facilitate lookup and creation of instance by their {@link String} getType.
	 */
	interface Factory {
		<E, P, R> EventExecutor<P, R> newInstance(final String contextName, final ExecutorOptions options, final Supplier<E> constructor, final Graph<E> graph,
											final BiConsumer<E, P> initializer, final Consumer<E> ackListener, final EventAccessor<E, P> eventAccessor,
											final StatsListener<E> statsListener, final boolean cleanShutdown, final Function<E, R> resultResolver);
	}

}