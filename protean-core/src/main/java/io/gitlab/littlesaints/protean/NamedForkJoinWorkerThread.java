/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinWorkerThread;

/**
 * @author Varun Anand
 * @since 1.0
 */
public class NamedForkJoinWorkerThread extends ForkJoinWorkerThread {

    private final String baseName;

    private final Runnable onClose;

    public NamedForkJoinWorkerThread(String baseName, ForkJoinPool pool, Runnable onClose) {
        super(pool);
        this.baseName = baseName;
        this.onClose = onClose;
    }

    public NamedForkJoinWorkerThread(String baseName, ForkJoinPool pool) {
        this(baseName, pool, () -> {});
    }

    @Override
    protected void onStart() {
        super.onStart();
        Thread.currentThread().setName(baseName + "::worker-" + getPoolIndex());
    }

    @Override
    protected void onTermination(Throwable exception) {
        super.onTermination(exception);
        this.onClose.run();
    }
}