/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.exchanges.executor;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import io.gitlab.littlesaints.protean.Constants;
import io.gitlab.littlesaints.protean.NamedForkJoinWorkerThread;
import io.gitlab.littlesaints.protean.Processor;
import io.gitlab.littlesaints.protean.configuration.Config;
import io.gitlab.littlesaints.protean.exchanges.MessageExchange;
import io.gitlab.littlesaints.protean.resources.Registrar;

/**
 * This executor performs un-ordered executions.
 * There's no order in execution for submitted objects.
 *
 * @param <E> The getType of objects to execute upon.
 * @author Varun Anand
 * @since 1.0
 */
public class UnOrderedExecutionExchange<E> extends AbstractExecutionExchange<E> {

	public static final String TYPE = "protean::exchange::execution::unordered";

	public static class _Registrar implements Registrar {
		static {
			MessageExchange.REGISTRY.register(TYPE, UnOrderedExecutionExchange::new);
		}

		@Override
		public String toString() {
			return "UnOrderedExecutionExchange::Registrar";
		}
	}

	private static final String nameSuffix = "::UnOrderedExecutionExchange";

	private final ExecutorService executor;
	private final Reader<E> reader;
	private final Reader<E>[] outputExchangeReaders;
	private final Writer<E>[] outputExchangeWriters;

	private final AtomicReferenceArray<MessageExchange<E>> inputExchanges;
	private final AtomicReferenceArray<MessageExchange<E>> outputExchanges;

	private class _WorkerThread extends NamedForkJoinWorkerThread {

		private final Consumer<E> action;

		_WorkerThread(String name, ForkJoinPool pool, Processor<E> processor) {
			super(name, pool, processor::close);
			this.action = processor.getFunction()::apply;
		}

		void execute(E e) {
			action.accept(e);
			outputExchangeWriters[getPoolIndex()].put(e);
		}
	}

	private UnOrderedExecutionExchange(final int capacity, final boolean isMultiProducer, final Config config) {
		this(config.getOrDefault(Constants.NAME, System.currentTimeMillis() + "") + nameSuffix, config.<Integer>getOrDefault(Constants.CONCURRENCY, Runtime.getRuntime().availableProcessors()), capacity,
			config.get(Constants.Exchange.EXCHANGE_SUPPLIER), config.get(Constants.Exchange.PROCESSOR_SUPPLIER));
	}

	/**
	 * Construct an executor
	 *
	 * @param name             the name of this exchange. Pass null, if no specific value is needed.
	 * @param concurrency      the 'fixed' number of threads to use
	 * @param capacity         the buffer size of each thread's message exchange
	 * @param exchangeSupplier the supplier function for creating a message exchange
	 * @param processorSupplier   the processorSupplier to perform on a submitted object/event
	 */
	private UnOrderedExecutionExchange(String name, int concurrency, int capacity, IntFunction<MessageExchange<E>> exchangeSupplier,
									   Supplier<Processor<E>> processorSupplier) {
		super(name, processorSupplier, concurrency, exchangeSupplier);

		inputExchanges = new AtomicReferenceArray<>(concurrency);
		outputExchanges = new AtomicReferenceArray<>(concurrency);

		reader = new _Reader();

		outputExchangeReaders = new Reader[concurrency];
		outputExchangeWriters = new Writer[concurrency];

		IntStream.range(0, concurrency).forEach(i -> {
			MessageExchange<E> exchange = exchangeSupplier.apply(capacity);
			outputExchangeReaders[i] = exchange.getReader();
			outputExchangeWriters[i] = exchange.newWriter();
			inputExchanges.set(i, exchange);
		});

		this.executor = new ForkJoinPool(concurrency, p -> new _WorkerThread(name, p, processorSupplier.get()), null, true);

	}

	private class _Writer implements Writer<E> {
		@Override
		public boolean put(final E e) {
		    //TODO:: can we do better ?
			executor.submit(() -> ((_WorkerThread) Thread.currentThread()).execute(e));
			return true;
		}
	}

	private class _Reader extends _ExecutionExchangeReader {
		@Override
		Reader<E> getOutputExchangeReader(int idx) {
			return outputExchangeReaders[idx];
		}
	}

	@Override
	public Writer<E> newWriter() {
		return new _Writer();
	}

	@Override
	public Reader<E> getReader() {
		return reader;
	}

	@Override
	public void shutdown() {
//		while (!isEmpty()) {
//			Thread.yield();
//		}
		IntStream.range(0, concurrency)
			.boxed()
			.map(inputExchanges::get)
			.filter(Objects::nonNull)
			.forEach(MessageExchange::shutdown);
		IntStream.range(0, concurrency)
			.boxed()
			.map(outputExchanges::get)
			.filter(Objects::nonNull)
			.forEach(MessageExchange::shutdown);
		executor.shutdown();
		while (!executor.isShutdown()) {
			try {
				executor.awaitTermination(1, TimeUnit.MILLISECONDS);
			} catch (InterruptedException e) {
				//TODO logging
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean isEmpty() {
		return IntStream.range(0, concurrency)
			.boxed()
			.map(inputExchanges::get)
			.filter(Objects::nonNull)
			.map(MessageExchange::isEmpty)
			.reduce((e1, e2) -> e1 && e2)
			.orElse(Boolean.FALSE);
	}
}