/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.gitlab.littlesaints.protean.Context;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Options to common a {@link Context}.
 *
 * @author Varun Anand
 * @since 1.0
 */
@Getter
@Setter
@ToString
public class ContextOptions {

	private String provider = Defaults.CONTEXT;

	private ExecutorOptions executor = new ExecutorOptions();

	private EventOptions event = new EventOptions();

	@JsonProperty("result-mapper")
	private String resultMapper = Defaults.RESULT_MAPPER;

	private GraphOptions workflow;

	private AnalyticsOptions analytics = new AnalyticsOptions();

	private ThrottleOptions throttle;

	@JsonProperty("clean-shutdown")
	private boolean cleanShutdown;
}