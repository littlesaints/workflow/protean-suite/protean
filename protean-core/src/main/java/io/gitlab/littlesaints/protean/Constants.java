/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean;

/**
 * @author Varun Anand
 * @since 1.0
 */
public interface Constants {

	String CONCURRENCY = "concurrency";

	String NAME = "name";

	interface Statistics {
		String E2E = "e2e";
	}

	interface Exchange {
		String ORDER_RESOLVER = "orderResolver";
		String PROCESSOR_SUPPLIER = "processorSupplier";
		String EXCHANGE_SUPPLIER = "exchangeSupplier";
		String LAZY_ROUTES = "lazyRoutes";
	}

	interface Context {
        String EXCEPTION_HANDLER = "ExceptionHandler";
        String FILTER = "Filter";
        String ROUTE_RESOLVER = "RouteResolver";
	}

}
