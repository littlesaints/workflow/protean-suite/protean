/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean;

import static io.gitlab.littlesaints.protean.Constants.Context.EXCEPTION_HANDLER;
import static io.gitlab.littlesaints.protean.Constants.Context.FILTER;
import static io.gitlab.littlesaints.protean.Constants.Context.ROUTE_RESOLVER;
import static com.littlesaints.protean.functions.maths.Mathematician.isPowerOfTwo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;

import com.littlesaints.protean.functions.streams.If;
import io.gitlab.littlesaints.protean.configuration.*;
import io.gitlab.littlesaints.protean.event.State;
import com.littlesaints.protean.functions.trial.Strategy;
import io.gitlab.littlesaints.protean.resources.ResourceLocator;

import lombok.*;

/**
 * Defines a set processing options and configurations for a task to be executed on an event.
 * Nodes are the building blocks for a processing {@link Graph} that represents the complete execution workflow or pipeline.
 *
 * @see Graph
 */
@ToString
@Getter
@EqualsAndHashCode(of = {"name"}, doNotUseGetters = true)
@Builder
public class Node<E> {

    @NonNull
    private final String name;

    @NonNull
    private final Supplier<Processor<E>> processorSupplier;

    private final boolean skippable;

    private final int concurrency;

    private final int buffer;

    private final BiFunction<E, Exception, State> exceptionHandler;

    private final Supplier<Predicate<E>> filter;

    private final ToIntFunction<E> routeResolver;

    private final Strategy retryStrategy;

    @Builder(builderClassName = "NodeConfigurer", builderMethodName = "configure", buildMethodName = "get")
    private static <E> Node<E> from(@NonNull Configuration configuration, @NonNull ResourceLocator locator, @NonNull NodeOptions options) {

        //TODO: add test for when no processor or invalid processor configured

        Collection<Supplier<Processor<E>>> processorSuppliers = new ArrayList<>();
        for (ProcessorOptions o : options.getProcessors()) {
            processorSuppliers.add(Processor.supplier(configuration, locator, o));
        }
        final Node<E> node = Node.<E>builder()
            .name(options.getName())
            .processorSupplier(processorSuppliers.stream()
                .reduce(Processor::compose)
                .orElse(null))
            .concurrency(options.getConcurrency())
            .buffer(options.getBuffer())
            .skippable(options.isSkippable())
            .retryStrategy(options.getRetryStrategy())
            .exceptionHandler(Optional.ofNullable(options.getExceptionHandler())
                .map(o -> locator.<Function<Config, BiFunction<E, Exception, State>>>lookupOrThrow(o.getSupplier(), EXCEPTION_HANDLER))
                .map(f -> f.apply(new Config(options.getExceptionHandler().getConfig())))
                .orElse(null))
//                .filter(locator.<Supplier<Predicate<E>>>optionalLookupOrThrow(options.getFilter(), FILTER).orElse(null))
            .filter(evaluateFilter(options.getFilter(), locator))
            .routeResolver(locator.<Function<Integer, ToIntFunction<E>>>optionalLookupOrThrow(options.getRouteResolver(), ROUTE_RESOLVER)
                .map(s -> s.apply(options.getConcurrency()))
                .orElse(null))
            .build();
        node.validate();
        return node;
    }

    //TODO: make a function
    private static <E> Supplier<Predicate<E>> evaluateFilter(GenericOptions options, ResourceLocator locator) {
        final Function<GenericOptions, Optional<Supplier<Predicate<E>>>> filterEvaluator = If.<GenericOptions, Optional<Supplier<Predicate<E>>>>test(f -> f.getConfig().isEmpty())
                .then(f -> locator.optionalLookupOrThrow(f.getSupplier(), FILTER))
                .orElse(f -> locator.<Function<Config, Predicate<E>>>optionalLookupOrThrow(f.getSupplier(), FILTER)
                        .map(fn -> () -> fn.apply(new Config(options.getConfig()))));
        return Optional.ofNullable(options)
                .flatMap(filterEvaluator)
                .orElse(null);
    }

    private void validate() {
		Objects.requireNonNull(name, "name must be defined !!");
		Objects.requireNonNull(processorSupplier, "processorSupplier must be defined !!");
        if (concurrency < 1) {
            throw new RuntimeException("concurrency must be greater than 0");
        }
        if (routeResolver != null) {
            if (concurrency == 1) {
                throw new RuntimeException("concurrency of 1 for ordered processing might cause missed processing for some events.");
            }
            if (!isPowerOfTwo.test(concurrency)) {
                throw new RuntimeException("concurrency must be a power of 2 for ordered processing.");
            }
        }
    }
}
