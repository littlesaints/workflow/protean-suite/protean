/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.exchanges.executor;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;
import java.util.stream.IntStream;

import io.gitlab.littlesaints.protean.Processor;
import io.gitlab.littlesaints.protean.exchanges.MessageExchange;

/**
 * This executor performs ordered executions.
 * The order of execution for submitted objects is determined by a orderResolver. This orderResolver is provided during the instantiation of this executor.
 *
 * @param <E> The getType of objects to execute upon.
 * @author Varun Anand
 * @since 1.0
 *
 */
class OrderedExecutionMultiExchange<E> extends AbstractExecutionExchange<E> {

    public static final String TYPE = "exchange::execution::java::ordered::multi";

    private static final String nameSuffix = "::OrderedExecutionExchange-multi";

    private final Reader<E> reader;

    private final AtomicReferenceArray<Thread> workers;
    private final AtomicReferenceArray<Runnable> onCloseOps;

    private final int bufferSize;
    private final boolean lazyRoutes;

    private final ToIntFunction<E> orderResolver;

    private final AtomicReferenceArray<Reader<E>> outputExchangeReaders;
    private final AtomicReferenceArray<Writer<E>> outputExchangeWriters;

    private final AtomicReferenceArray<Reader<E>> inputExchangeReaders;
    private final AtomicReferenceArray<Writer<E>> inputExchangeWriters;

    private final AtomicReferenceArray<MessageExchange<E>> inputExchanges;
    private final AtomicReferenceArray<MessageExchange<E>> outputExchanges;

    /**
     * Construct an executor
     *
     * @param name             the name of this exchange. Pass null, if no specific value is needed.
     * @param concurrency      the 'fixed' number of threads to use
     * @param bufferSize       the buffer size of each thread's message exchange
     * @param exchangeSupplier the supplier function for creating a message exchange
     * @param processorSupplier   the action to perform on a submitted object/event
     * @param orderResolver    the function to resolve the order of execution for submitted objects/events.
     */
    OrderedExecutionMultiExchange(String name, int concurrency, int bufferSize, IntFunction<MessageExchange<E>> exchangeSupplier, Supplier<Processor<E>> processorSupplier,
                                  ToIntFunction<E> orderResolver, boolean lazyRoutes) {
        super(name + nameSuffix, processorSupplier, concurrency, exchangeSupplier);
        this.bufferSize = bufferSize;
        this.orderResolver = orderResolver;
        inputExchanges = new AtomicReferenceArray<>(concurrency);
        outputExchanges = new AtomicReferenceArray<>(concurrency);

        reader = new _Reader();
        workers = new AtomicReferenceArray<>(concurrency);
        onCloseOps = new AtomicReferenceArray<>(concurrency);

        outputExchangeReaders = new AtomicReferenceArray<>(concurrency);
        outputExchangeWriters = new AtomicReferenceArray<>(concurrency);
        inputExchangeReaders = new AtomicReferenceArray<>(concurrency);
        inputExchangeWriters = new AtomicReferenceArray<>(concurrency);

        this.lazyRoutes = lazyRoutes;
        if (!lazyRoutes) {
            IntStream.range(0, concurrency).forEach(r -> {
                this.initRoute(r);
                this.initRouteProcessor(r);
            });
        }
    }

    @Override
    public Reader<E> getReader() {
        return reader;
    }

    @Override
    public Writer<E> newWriter() {
        return new _Writer();
    }

    private class _Reader extends _ExecutionExchangeReader {
        @Override
        Reader<E> getOutputExchangeReader(int idx) {
            return outputExchangeReaders.get(idx);
        }
    }

    private class _Writer implements Writer<E> {
        @Override
        public boolean put(final E e) {
            int route = orderResolver.applyAsInt(e) & (concurrency - 1);
            if (lazyRoutes) {
                initRoute(route);
            }
            initRouteProcessor(route);
            inputExchangeWriters.get(route).put(e);
            return true;
        }
    }

    private void initRoute(int route) {
        MessageExchange<E> inputExchange = exchangeSupplier.apply(bufferSize);
        if (inputExchangeReaders.compareAndSet(route, null, inputExchange.getReader())) {
            inputExchangeWriters.lazySet(route, inputExchange.newWriter());

            MessageExchange<E> outputExchange = exchangeSupplier.apply(bufferSize);
            outputExchangeReaders.lazySet(route, outputExchange.getReader());
            outputExchangeWriters.lazySet(route, outputExchange.newWriter());

            inputExchanges.set(route, inputExchange);
            outputExchanges.set(route, outputExchange);
        } else {
            while (outputExchangeWriters.get(route) == null) {
                Thread.yield();
            }
        }
    }

    //TODO watcher for thread liveness

    private void initRouteProcessor(int route) {

        Thread worker = workers.get(route);
        if (worker == null || !worker.isAlive()) {
            final Reader<E> inputExchangeReader = inputExchangeReaders.get(route);
            final Writer<E> outputExchangeWriter = outputExchangeWriters.get(route);
            final Processor<E> processor = processorSupplier.get();
            final Consumer<E> action = processor.getFunction()::apply;
            final Thread newWorker = new Thread(() ->
                    inputExchangeReader.take(e -> {
                        action.accept(e);
                        outputExchangeWriter.put(e);
                        return true;
                    }), getName() + "-worker-" + route);

            if (workers.compareAndSet(route, worker, newWorker)) {
                if (worker != null) {
                    onCloseOps.get(route).run();
                }
                onCloseOps.lazySet(route, processor::close);
                newWorker.start();
            }
        }
    }

    @Override
    public void shutdown() {
        IntStream.range(0, concurrency)
                .boxed()
                .map(inputExchanges::get)
                .filter(Objects::nonNull)
                .forEach(MessageExchange::shutdown);
        IntStream.range(0, concurrency)
                .boxed()
                .map(outputExchanges::get)
                .filter(Objects::nonNull)
                .forEach(MessageExchange::shutdown);
        IntStream.range(0, concurrency).boxed()
                .map(workers::get)
                .filter(Objects::nonNull)
                .forEach(Thread::interrupt);
        IntStream.range(0, concurrency).boxed()
                .map(onCloseOps::get)
                .filter(Objects::nonNull)
                .forEach(Runnable::run);
    }

    @Override
    public boolean isEmpty() {
        return IntStream.range(0, concurrency)
                .boxed()
                .map(inputExchanges::get)
                .filter(Objects::nonNull)
                .map(MessageExchange::isEmpty)
                .reduce((e1, e2) -> e1 && e2)
                .orElse(Boolean.FALSE);
    }

}
