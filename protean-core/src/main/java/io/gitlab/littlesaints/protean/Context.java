/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean;

import io.gitlab.littlesaints.protean.analytics.AggregateStatsListener;
import io.gitlab.littlesaints.protean.configuration.Configuration;
import io.gitlab.littlesaints.protean.resources.Registry;
import io.gitlab.littlesaints.protean.throttle.Throttle;

import java.util.function.Function;

/**
 * This represents a complete execution workflow / pipeline.
 * It should be used by the application to retrieve an endpoint using {@link #getEndpoint()}, to submit payloads for execution.
 * <p>
 * It can be built using a {@link Configuration}.
 *
 * @param <P> the getType of payload that'll be submitted.
 * @param <R> the getType of result that will be returned.
 * @author Varun Anand
 * @since 1.0
 */
public interface Context<P, R> {

    Registry<Factory> REGISTRY = new Registry<>();

    interface Factory {
        <E, P, R> Context<P, R> newInstance(String name, Configuration configuration, final AggregateStatsListener<E> statsListener,
                                            final EventExecutor<P, R> executor, final Throttle throttle);
    }

    void shutdown();

    Function<P, R> getEndpoint();

}
