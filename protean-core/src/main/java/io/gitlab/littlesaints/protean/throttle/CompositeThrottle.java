/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.throttle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * A composite {@link Throttle} composed of other throttles. A throttle kicks in when all included throttles are active and gets inactive when all it's constituents are inactive.
 *
 * @author Varun Anand
 * @since 1.0
 */
public class CompositeThrottle implements Throttle {

	private final Collection<Throttle> throttles;

	public CompositeThrottle(Throttle... throttles) {
		this.throttles = Arrays.stream(throttles).collect(Collectors.toCollection(ArrayList::new));
	}

	@Override
	public boolean isActive() {
		return throttles.stream().anyMatch(Throttle::isActive);
	}

	@Override
	public void acquire() {
		throttles.forEach(Throttle::acquire);
	}

	@Override
	public void release() {
		throttles.forEach(Throttle::release);
	}
}
