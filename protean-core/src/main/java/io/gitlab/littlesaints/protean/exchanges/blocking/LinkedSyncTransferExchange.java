/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.exchanges.blocking;

import io.gitlab.littlesaints.protean.configuration.Config;
import io.gitlab.littlesaints.protean.exchanges.MessageExchange;
import io.gitlab.littlesaints.protean.resources.Registrar;

import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TransferQueue;

/**
 * A {@link TransferQueue} based {@link MessageExchange} that uses a {@link LinkedTransferQueue}
 *
 * @param <E> the getType of elements
 */
public class LinkedSyncTransferExchange<E> extends AbstractBlockingExchange<TransferQueue<E>, E> {

	public static final String TYPE = "protean::exchange::java::linkedTransferQ::sync";

	public static class _Registrar implements Registrar {
		static {
			REGISTRY.register(TYPE, LinkedSyncTransferExchange::new);
		}

		@Override
		public String toString() {
			return "LinkedSyncTransferExchange::Registrar";
		}
	}

	private final Writer<E> writer;

	@Override
	public Writer<E> newWriter() {
		return writer;
	}

	private LinkedSyncTransferExchange(final int capacity, final boolean isMultiProducer, final Config config) {
		super(new LinkedTransferQueue<>());
		this.writer = new LinkedTransferWriter();
	}

	private class LinkedTransferWriter implements Writer<E> {
		@Override
		public boolean put(E e) {
			while (true) {
				try {
					queue.transfer(e);
					break;
				} catch (InterruptedException x) {
					x.printStackTrace();
				}
			}
			return true;
		}
	}
}
