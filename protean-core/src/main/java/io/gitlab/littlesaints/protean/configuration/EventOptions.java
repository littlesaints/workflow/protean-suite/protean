/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Options to common an Event's instance.
 *
 * @author Varun Anand
 * @since 1.0
 */
@Getter
@Setter
@ToString
public class EventOptions {

	private GenericOptions type = GenericOptions.builder().supplier(Defaults.EVENT_SUPPLIER).build();

	private String accessor = Defaults.EVENT_ACCESSOR;

	@JsonProperty("reference-resolver")
	private String referenceIdResolver;

	//todo: caching
	private CacheOptions cache;

}
