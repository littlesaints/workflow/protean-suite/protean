/*
 *                     protean-core
 *              Copyright (C) 2018 Varun Anand
 *
 * This file is part of protean-core.
 *
 * protean-core is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * protean-core is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.littlesaints.protean.resources;

import io.gitlab.littlesaints.protean.configuration.Config;
import io.gitlab.littlesaints.protean.configuration.Props;
import io.gitlab.littlesaints.protean.event.*;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

/**
 * @author Varun Anand
 * @since 1.0
 */
public interface ProteanResources {

	String GC = "protean::runnable::java::runtime::gc";
	String THREAD_SAFE_EVENT_SUPPLIER = "protean::event::supplier::thread-safe-event";
	String SINGLE_THREAD_EVENT_SUPPLIER = "protean::event::supplier::single-thread-event";
	String SIMPLE_EVENT_ACCESSOR = "protean::event::accessor::simple-event";
	String CONTEXT_RESULT_MAPPER = "protean::context::result-mapper";

	class _Registrar implements Registrar {
		static {
			ResourceLocator.REGISTRY.register("_PROTEAN", new Locator());
		}

		@Override
		public String toString() {
			return "ProteanResources::Registrar";
		}
	}

	class Locator implements ResourceLocator {

		private final Props resources;

		private Locator() {
			resources = new Props();
			resources.put(ProteanResources.GC, newGCRunnable());
			resources.put(ProteanResources.THREAD_SAFE_EVENT_SUPPLIER, threadSafeEventSupplier());
			resources.put(ProteanResources.SINGLE_THREAD_EVENT_SUPPLIER, singleThreadEventSupplier());
			resources.put(ProteanResources.SIMPLE_EVENT_ACCESSOR, simpleEventAccessor());
			resources.put(ProteanResources.CONTEXT_RESULT_MAPPER, defaultResultMapper());
		}

		private <E> Function<E, E> defaultResultMapper() {
			return e -> e;
		}

		private Runnable newGCRunnable() {
			final Runtime runtime = Runtime.getRuntime();
			return runtime::gc;
		}

		@Override
		public <T> Optional<T> doLookup(String name) {
			return Optional.ofNullable(resources.get(name));
		}

		private Function<Config, ?> threadSafeEventSupplier() {
			return ThreadSafeEvent::new;
		}

		private Function<Config, ?> singleThreadEventSupplier() {
			return SingleThreadEvent::new;
		}

		private EventAccessor<SimpleEvent, ?> simpleEventAccessor() {
			final AtomicLong sequence = new AtomicLong();
			return new EventAccessor<SimpleEvent, Object>() {

				@Override
				public long getIdentifier(SimpleEvent event) {
					return event.getIdentifier();
				}

				@Override
				public void setState(SimpleEvent event, State state) {
					event.setState(state);
				}

				@Override
				public State getState(SimpleEvent event) {
					return event.getState();
				}

				@Override
				public long getStartEpochMillis(SimpleEvent event) {
					return event.getStartEpochMillis();
				}

				@Override
				public void initialize(SimpleEvent event, Object payload) {
					event.init(sequence::incrementAndGet);
					event.setPayload(payload);
				}

				@Override
				public <T> T getConf(SimpleEvent event, String key) {
					return null;
				}
			};
		}
	}
}