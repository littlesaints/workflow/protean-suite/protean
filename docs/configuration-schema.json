{
  "title": "configuration",
  "description": "Protean configuration",
  "type": "object",
  "additionalProperties": false,
  "properties": {
    "contexts": {
      "type": "object",
      "additionalProperties": {
        "type": "object",
        "properties": {
          "provider": {
            "type": "string"
          },
          "executor": {
            "$ref": "#/definitions/executor"
          },
          "event": {
            "$ref": "#/definitions/event"
          },
          "throttle": {
            "$ref": "#/definitions/throttle"
          },
          "analytics": {
            "$ref": "#/definitions/analytics"
          },
          "result-mapper": {
            "type": "string"
          },
          "workflow": {
            "$ref": "#/definitions/graph"
          },
          "clean-shutdown": {
            "type": "boolean"
          }
        },
        "required": [
          "workflow"
        ]
      }
    }
  },
  "required": [
    "contexts"
  ],
  "definitions": {
    "provider": {
      "type": "object",
      "minProperties": 1,
      "properties": {
        "supplier": {
          "type": "string"
        },
        "config": {
          "type": "object",
          "additionalProperties": {
            "anyOf": [
              {
                "type": "string"
              },
              {
                "type": "integer"
              },
              {
                "type": "number"
              },
              {
                "type": "boolean"
              }
            ]
          }
        }
      },
      "required": [
        "supplier"
      ]
    },
    "processor": {
      "allOf": [{
        "$ref": "#/definitions/provider"
      }],
      "properties": {
        "contexts": {
          "type": "array",
          "additionalProperties": false,
          "minItems": 1,
          "uniqueItems": true,
          "items": [
            {
              "type": "string"
            }
          ]
        },
        "tools": {
          "type": "string"
        },
        "on-close": {
          "type": "string"
        }
      }
    },
    "executor": {
      "type": "object",
      "properties": {
        "type": {
          "$ref": "#/definitions/provider"
        },
        "multi-producer": {
          "type": "boolean"
        },
        "buffer": {
          "type": "integer"
        },
        "exchange": {
          "$ref": "#/definitions/provider"
        },
        "mode": {
          "type": "object",
          "properties": {
            "ordered": {
              "$ref": "#/definitions/provider"
            },
            "un-ordered": {
              "$ref": "#/definitions/provider"
            },
            "force-defaults": {
              "type": "boolean"
            }
          },
          "additionalProperties": false
        }
      },
      "additionalProperties": false
    },
    "event": {
      "type": "object",
      "properties": {
        "type": {
          "$ref": "#/definitions/provider"
        },
        "accessor": {
          "type": "string"
        },
        "cache": {
          "$ref": "#/definitions/cache"
        },
        "reference-resolver": {
          "type": "string"
        }
      },
      "additionalProperties": false
    },
    "cache": {
      "type": "object",
      "properties": {
        "type": {
          "$ref": "#/definitions/provider"
        },
        "size": {
          "type": "integer"
        }
      },
      "additionalProperties": false
    },
    "listeners": {
      "type": "array",
      "additionalProperties": false,
      "minItems": 1,
      "uniqueItems": true,
      "items": [
        {
          "$ref": "#/definitions/provider"
        }
      ]
    },
    "processors": {
      "type": "array",
      "additionalProperties": false,
      "minItems": 1,
      "uniqueItems": true,
      "items": [
        {
          "$ref": "#/definitions/processor"
        }
      ]
    },
    "throttle": {
      "type": "object",
      "properties": {
        "type": {
          "$ref": "#/definitions/provider"
        },
        "listeners": {
          "$ref": "#/definitions/listeners"
        }
      },
      "additionalProperties": false
    },
    "analytics": {
      "type": "object",
      "properties": {
        "statistics": {
          "type": "boolean"
        },
        "listeners": {
          "$ref": "#/definitions/listeners"
        }
      },
      "additionalProperties": false
    },
    "retry-strategy": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "maxYieldRetries": {
          "type": "integer"
        },
        "maxDelayRetries": {
          "type": "integer"
        },
        "delayInMillis": {
          "type": "integer"
        },
        "delayThresholdInMillis": {
          "type": "integer"
        },
        "delayIncreaseRetries": {
          "type": "integer"
        }
      }
    },
    "graph": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "stages": {
          "type": "array",
          "additionalProperties": false,
          "minItems": 1,
          "uniqueItems": true,
          "items": [
            {
              "$ref": "#/definitions/node"
            }
          ]
        },
        "dependencies": {
          "type": "object",
          "additionalProperties": {
            "type": "array",
            "additionalProperties": false,
            "minItems": 1,
            "uniqueItems": true,
            "items": [
              {
                "type": "string"
              }
            ]
          }
        }
      }
    },
    "node": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "name": {
          "type": "string"
        },
        "skippable": {
          "type": "boolean"
        },
        "buffer": {
          "type": "integer"
        },
        "filter": {
          "type": "string"
        },
        "exception-handler": {
          "type": "string"
        },
        "route-resolver": {
          "type": "string"
        },
        "concurrency": {
          "type": "integer"
        },
        "processors": {
          "$ref": "#/definitions/processors"
        },
        "retry-strategy": {
          "$ref": "#/definitions/retry-strategy"
        }
      },
      "required": [
        "name",
        "processors"
      ]
    }
  }
}